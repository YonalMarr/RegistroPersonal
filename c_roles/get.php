<?php
	include '../home/user_validate.php';
	
	$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	
    $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'auth_group.id';  
    $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC'; 
	
    $criteria=isset($_POST['criteria']) ? pg_escape_string(strtoupper($_POST['criteria'])) : '';
	
	$offset = ($page-1)*$rows;
	
	$result = array();

	if($criteria !== ""){
		$where = " 
		(
		auth_group.id  LIKE '%".$criteria."%'


		)  AND auth_group.id > 0";
	}
	$rs = pg_query("SELECT
auth_group.role as rol,
auth_group.description as descripcion,
auth_group.id
FROM
auth_group
".$where);
	
	$row = pg_fetch_row($rs);
	
	$result["total"] = pg_num_rows($rs);
	
	$rs = pg_query("
	SELECT
auth_group.role as rol,
auth_group.description as descripcion,
auth_group.id
FROM
auth_group ".$where." 
	ORDER BY $sort $order LIMIT $rows OFFSET $offset");
	
	$items = array();
	while ($row = pg_fetch_assoc($rs)) {
		
		$row['nombre']=strtoupper(utf8_decode($row['nombre']));
		$row['apellido']=strtoupper(utf8_decode($row['apellido']));
		
		$items[] = array_map('utf8_encode', $row);
	}	
	$result["rows"] = $items;
	
	echo json_encode($result);
?>