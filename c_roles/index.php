<?php include '../home/head.php'; ?>

    
	<script type="text/javascript">
		var url;
		var read_noly;
		var esta_global;
		var id;
		


		
//	##########################################################################################################################		
		function newRecord(){
			$('#dlg').dialog('open').dialog('setTitle','Nuevo');
			$('#fm').form('clear');
			
	$('#roles').textbox('readonly', false);
				$('#descripcion').textbox('readonly', false);
			
			$('#guardado2').hide();			
			
			$('#btnaceptar').linkbutton('enable');
			
			read_noly = 0;
			esta_global = 0;	
		}
		
		function editRecord(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$('#dlg').dialog('open').dialog('setTitle','Modificar');
				
				$('#roles').textbox('readonly', true);
				$('#descripcion').textbox('readonly', false);
				
				$('#roles').textbox('setValue',row.rol);
				$('#descripcion').textbox('setValue',row.description);

				$('#btnaceptar').linkbutton('enable');

				$('#fm').form('load',row);
				id = row.id;
			}
			read_noly = 1;
			esta_global = 1;
		}
//	##########################################################################################################################		
		function saveRecord(){
	//		**********************************************************	
			if($('#roles').textbox('isValid') == false){
				rojo(2);
				$('#guardado2').html('Ingrese los roles');
				$('#roles').textbox('textbox').focus();
				return false;
			}
			if($('#descripcion').textbox('isValid') == false){
				rojo(2);
				$('#guardado2').html('Ingrese descripcion');
				$('#descripcion').textbox('textbox').focus();
				return false;
			}
			
		

//			**********************************************************		
			if(esta_global == 1){				
				$('#fm').form('submit',{
					url: 'update.php?id='+id,
					// success:function(result){
					// 	alert(result);
					// }
					success: function(result){
						var result = eval('('+result+')');
						if (result.success){
							$('#dlg').dialog('close');		// close the dialog
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({
								title: 'Error',
								msg: result.msg
							});
						}
					}
				});
				verde(2);
				$('#guardado2').html('El registro ha sido modificado satisfactoriamente');
			}else{
				
				$('#fm').form('submit',{
					url: 'save.php',
					success: function(result){
						var result = eval('('+result+')');
										
						if (result.success){
							$('#dlg').dialog('close');		// close the dialog
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({
								title: 'Error',
								msg: result.msg
							});
						}
					}
				});
				
				verde(2);
				$('#guardado2').html('El registro ha sido guardado satisfactoriamente');
			}
		}
//	##########################################################################################################################		
		function removeRecord(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirmaci&oacute;n','Est&aacute; usted seguro de eliminar este registro?',function(r){
					if (r){
						$.post('remove.php',{id:row.id},function(result){
							if (result.success){
								$('#dg').datagrid('reload');	// reload the user data
							} else {
								$.messager.alert('Error',result.msg,'error');
							}
						},'json');
					}
				});
			}
		}
//	##########################################################################################################################		
		function buscarCodigo()
		{
			var str = $('#correo').textbox('getValue');
						
			if(read_noly==1){	//	edicion
				exit();
			}

			if (str==""){
				$('#buscar').html('');
				$('#btnaceptar').linkbutton('enable');
				return;
			}
			
			if (str==0){
				$('#buscar').html('');
				$('#btnaceptar').linkbutton('disable');
				return;
			}
			
			$.post('find.php?correo='+str,
			function(data){
				$('#buscar').show();
				if(data==1){
					rojo(2);
					$('#guardado2').html('Este Correo ya existe');
					$('#btnaceptar').linkbutton('disable');
				}else{
					verde(2);
					$('#guardado2').html('disponible');
					$('#btnaceptar').linkbutton('enable');
				}
			});
		}
//	##########################################################################################################################	
//	##########################################################################################################################		
		function buscarCedula()
		{ 


			//alert('asdasdasdas');
			var str = $('#cedula').numberbox('getValue');

			//alert(str);
						
			if(read_noly==1){	//	edicion
				exit();
			}

			if (str==""){
				$('#guardado2').html('');
				$('#btnaceptar').linkbutton('enable');
				return;
			}
			
			if (str==0){
				$('#guardado2').html('');
				$('#btnaceptar').linkbutton('disable');
				return;
			}
			
			$.post('findcedula.php?cedula='+str,
			function(data){
				$('#buscar').show();
				if(data==1){
					rojo(2);
					$('#guardado2').html('Esta Cedula ya existe');
					$('#btnaceptar').linkbutton('disable');
				}else{
					verde(2);
					$('#guardado2').html('disponible');
					$('#btnaceptar').linkbutton('enable');
				}
			});
		}
//	##########################################################################################################################		

	function verde(num){
		$('#guardado'+num).css('font-size','14px');
		$('#guardado'+num).css('padding','5px');
		
		$('#guardado'+num).css('border-color','#00FF00');
		$('#guardado'+num).css('background-color','#F0FFF0');
		$('#guardado'+num).css('display','block');
		$("#guardado"+num).fadeOut(4000);
	}
//	##########################################################################################################################		
	function rojo(num){
		$('#guardado'+num).css('font-size','14px');
		$('#guardado'+num).css('padding','5px');
		
		$('#guardado'+num).css('border-color','#FF0000');
		$('#guardado'+num).css('background-color','#FDE9D9');
		$('#guardado'+num).css('display','block');
		//$("#guardado"+num).fadeOut(3000);
	}
//	##########################################################################################################################	
//	##########################################################################################################################		
		function printRecord(){
			window.open('../reports/rpt_usuario.php', '', 'height=800,width=800,location=0');
		}
//	##########################################################################################################################		

	</script>
<?php include '../home/head_2.php'; ?>

        <div id="main" name="main" data-options="region:'center'">
            <table id="dg" title="Usuario" width="100%" align="center" class="easyui-datagrid"
                url="get.php" fit="true"
                toolbar="#toolbar" pagination="true" striped="true"
                rownumbers="false" fitColumns="true" singleSelect="true"
                data-options="
					    fit:true,
					    nowrap:false,
					    singleSelect:true,
					    fitColumns:true
    				"                    
                >
            <thead>
                <tr>
                        <th field="id" width="20%" sortable="true">ID</th>
                    	<th field="rol" width="30%" sortable="true">ROLES</th>
                    	<th field="descripcion" width="50%" sortable="true">DESCRIPCION</th>           
                </tr>
            </thead>
        </table>
        
	<div style="display:none">
        <div id="toolbar">
            <a class="easyui-linkbutton" iconCls="icon-newdoc" plain="true" onclick="newRecord()">Nuevo</a>
            <a class="easyui-linkbutton" iconCls="icon-modify" plain="true" onclick="editRecord()">Modificar</a>
            <a class="easyui-linkbutton" iconCls="icon-erase" plain="true" onclick="removeRecord()">Eliminar</a>


            <div id="busqueda" style="float:right">
            <input id="criteria" name="criteria" style="width:400px" data-options="
                    prompt: 'escriba la informaci&oacute;n a buscar...',
                    icons:[{
                        iconCls:'icon-search',
                        handler: function(e){

                            $('#dg').datagrid('load',
                            {
                                criteria:$('#criteria').textbox('getValue')
                            });
                          
                        }
                    }]
                    ">
            <script>
                $.extend($.fn.textbox.methods, {
                    addClearBtn: function(jq, iconCls){
                        return jq.each(function(){
                            var t = $(this);
                            var opts = t.textbox('options');
                            opts.icons = opts.icons || [];
                            opts.icons.unshift({
                                iconCls: iconCls,
                                handler: function(e){
                                    $(e.data.target).textbox('clear').textbox('textbox').focus();
                                    $(this).css('visibility','hidden');
									
									$('#dg').datagrid('load',
									{
										criteria:$('#criteria').textbox('getValue')
									});
                                }
                            });
                            t.textbox();
                            if (!t.textbox('getText')){
                                t.textbox('getIcon',0).css('visibility','hidden');
                            }
                            t.textbox('textbox').bind('keyup', function(){
                                var icon = t.textbox('getIcon',0);
                                if ($(this).val()){
                                    icon.css('visibility','visible');
                                } else {
                                    icon.css('visibility','hidden');
                               }
                            });
                        });
                    }
                });
                
                $(function(){
                    $('#criteria').textbox().textbox('addClearBtn', 'icon-clear');
                });
            </script>
            </div>
        </div>
        
        <div id="dlg" class="easyui-dialog" style="width:600px;height:600px;padding:10px 20px; position:relative;" closed="true" modal="true" buttons="#dlg-buttons">
            <div class="ftitle">Informaci&oacute;n del Usuario</div>
            <form id="fm" method="post" novalidate autocomplete="off">
            	<div class="fitem">
                    <label>Roles</label>
                    <input id="roles" name="roles" class="easyui-textbox" style="height:20px; width:300px" required="true">
                </div>
                      	<div class="fitem">
                    <label>Descripcion</label>
                    <input id="descripcion" name="descripcion" class="easyui-textbox" style="height:20px; width:300px" required="true">
                </div>
                
         </div>
                </form>
            <div id="guardado2" align="center" style="border-style:solid; font:Verdana, Geneva, sans-serif; border-width:1px; border-color:#00FF00; background-color:#F0FFF0; bottom:5px; width:92%; z-index:0; position:absolute; border-spacing:inherit; font-size:14px; display:none; padding:5px;"></div>
        </div>
        <div id="dlg-buttons">
            <a id="btnaceptar" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveRecord()">Guardar</a>
            <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Cancelar</a>
        </div>
        
        
        <div id="dlg4" class="easyui-dialog" style="width:500px;height:200px;padding:10px 20px; position:relative;" closed="true" modal="true" buttons="#dlg4-buttons"/> 
   <form enctype="multipart/form-data" id="fm4" method="post" novalidate autocomplete="off">

		   	  	<div class="fitem">
					<label>Sector</label><br>
					<input id="sector" name="sector" class="easyui-textbox" style=" width:200px; height:20px;font-size:10px;" required="true">
		   </div>

  
  </form>


  <div id="guardado7" align="center" style="border-style:solid; font:Verdana, Geneva, sans-serif; border-width:1px; border-color:#00FF00; background-color:#F0FFF0; bottom:5px; z-index:0; position:absolute; border-spacing:inherit; font-size:12px;padding:5px;display:none;">a</div>
	<div id="dlg4-buttons">
   <a id="btnacepta" class="easyui-linkbutton" iconCls="icon-ok" onclick="save_sector()">Guardar</a>
   <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg4').dialog('close')">Cancelar</a>
   </div>
  </div>
        <div id="pie"><?php// include("../home/pie.php"); ?></div>
            </div>
        </div>
    </div>
</body>
</html>