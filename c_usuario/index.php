<?php include '../home/head.php'; ?>

    
	<script type="text/javascript">
		var url;
		var read_noly;
		var esta_global;
		var id;
				
	function newRecord(){
			$('#dlg').dialog('open').dialog('setTitle','Nuevo');
			$('#fm').form('clear');
		
				$('#correo').textbox('readonly', false);
				$('#cedula').textbox('readonly', false);
				$('#contrasena').textbox('readonly', false);
				
			$('#guardado2').hide();			
			$('#btnaceptar').linkbutton('enable');
			
			read_noly = 0;
			esta_global = 0;
		}
	
	function editRecord(){
			var row = $('#dg').datagrid('getSelected');
			if (row)
			{
				$('#dlg').dialog('open').dialog('setTitle','Modificar');

				$('#correo').textbox('readonly', true);
				$('#cedula').textbox('readonly', true);
				$('#contrasena').textbox('setValue', true);
				$('#nombre').textbox('setValue',row.first_name);
				$('#apellido').textbox('setValue',row.last_name);
				$('#telefono').textbox('setValue',row.mobilenumber);
				$('#correo').textbox('setValue',row.email);
				$('#permiso').combobox('setValue',row.tipousuario);
				$('#sector').combobox('setValue',row.sector);
				
				$('#buscar').html('');
				$('#btnaceptar').linkbutton('enable');

				$('#fm').form('load',row);
				id = row.id;
			}
			read_noly = 1;
			esta_global = 1;
		}
//	##########################################################################################################################		
		function saveRecord(){
			if($('#nombre').textbox('isValid') == false){
				rojo(2);
				$('#guardado2').html('Ingrese el nombres del Usuario');
				$('#nombre').textbox('textbox').focus();
				return false;
			}
			if($('#apellido').textbox('isValid') == false){
				rojo(2);
				$('#guardado2').html('Ingrese el apellido del usuario');
				$('#apellido').textbox('textbox').focus();
				return false;
			}
			
			if($('#cedula').numberbox('isValid') == false){
				rojo(2);
				$('#guardado2').html('Ingrese cedula del Usuario');
				$('#cedula').numberbox('textbox').focus();
				return false;
			}

			if($('#correo').validatebox('isValid') == false){
				rojo(2);
				$('#guardado2').html('Ingrese el correo electr&oacute;nico');
				$('#correo').textbox('textbox').focus();
				return false;
			}
			if($('#contrasena').textbox('isValid') == false){
				rojo(2);
				$('#guardado2').html('La contraseña');
				$('#contrasena').textbox('textbox').focus();
				return false;
			}
			if($('#permiso').textbox('isValid') == false){
				rojo(2);
				$('#guardado2').html('permiso');
				$('#permiso').combobox('textbox').focus();
				return false;
			}
		

//			**********************************************************		
			if(esta_global == 1){				
				$('#fm').form('submit',{
					url: 'update.php?id='+id,
					success: function(result){
						var result = eval('('+result+')');
						if (result.success){
							$('#dlg').dialog('close');		// close the dialog
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({
								title: 'Error',
								msg: result.msg
							});
						}
					}
				});
				verde(2);
				$('#guardado2').html('El registro ha sido modificado satisfactoriamente');
			}else{
				
				$('#fm').form('submit',{
					url: 'save.php',
					success: function(result){
						var result = eval('('+result+')');
										
						if (result.success){
							$('#dlg').dialog('close');		// close the dialog
							$('#dg').datagrid('reload');	// reload the user data
						} else {
							$.messager.show({
								title: 'Error',
								msg: result.msg
							});
						}
					}
				});
				
				
			}
		}
//	##########################################################################################################################		
function removeRecord(){
			var row = $('#dg').datagrid('getSelected');
			if (row){
				$.messager.confirm('Confirmaci&oacute;n','Est&aacute; usted seguro de eliminar este registro?',function(r){
					if (r){
						$.post('remove.php',{id:row.id},function(result){
							if (result.success){
								$('#dg').datagrid('reload');	// reload the user data
							} else {
								$.messager.alert('Error',result.msg,'error');
							}
						},'json');
					}
				});
			}
		}
		
var gloarra=[0,0];
		function buscarCodigo()
		{
			var str = $('#correo').textbox('getValue');
						
			if(read_noly==1){	//	edicion
				exit();
			}

			if (str==""){
				$('#buscar').html('');
				$('#btnaceptar').linkbutton('enable');
				return;
			}
			
			if (str==0){
				$('#buscar').html('');
				$('#btnaceptar').linkbutton('disable');
				return;
			}
			
			$.post('find.php?correo='+str,
			function(data){
				$('#buscar').show();
				if(data==1){
					rojo(2);
					$('#guardado2').html('Este Correo ya existe');
					$('#btnaceptar').linkbutton('disable');
					gloarra[0]=1
				}else{
					verde(2);
					$('#guardado2').html('disponible');
					$('#btnaceptar').linkbutton('enable');
					gloarra[0]=4
				}
				if(gloarra[0]==1 || gloarra[1]==1 )
				{ 
					if(gloarra[0]==1){rojo(2);
					$('#guardado2').html('Este Correo ya existe');
					$('#btnaceptar').linkbutton('disable');}
					if(gloarra[1]==1){rojo(2);
					$('#guardado2').html('Esta Cedula ya existe');
					$('#btnaceptar').linkbutton('disable');}
				}
				if(gloarra[0]==4 && gloarra[1]==4 )
				{
					$('#guardado2').html('Datos correctos');
					$('#btnaceptar').linkbutton('enable');

				}
			});
		}


	function verde(num){
		$('#guardado'+num).css('font-size','14px');
		$('#guardado'+num).css('padding','5px');
		
		$('#guardado'+num).css('border-color','#00FF00');
		$('#guardado'+num).css('background-color','#F0FFF0');
		$('#guardado'+num).css('display','block');
		$("#guardado"+num).fadeOut(4000);
	}
//	##########################################################################################################################		
	function rojo(num){
		$('#guardado'+num).css('font-size','14px');
		$('#guardado'+num).css('padding','5px');
		
		$('#guardado'+num).css('border-color','#FF0000');
		$('#guardado'+num).css('background-color','#FDE9D9');
		$('#guardado'+num).css('display','block');
		//$("#guardado"+num).fadeOut(3000);
	}
//	##########################################################################################################################	
//	##########################################################################################################################		
		function printRecord(){
			window.open('../reports/rpt_usuario.php', '', 'height=800,width=800,location=0');
		}
//	##########################################################################################################################		

var gloarra=[0,0];
		function buscarCodigo()
		{
			var str = $('#correo').textbox('getValue');
						
			if(read_noly==1){	//	edicion
				exit();
			}

			if (str==""){
				$('#buscar').html('');
				$('#btnaceptar').linkbutton('enable');
				return;
			}
			
			if (str==0){
				$('#buscar').html('');
				$('#btnaceptar').linkbutton('disable');
				return;
			}
			
			$.post('find.php?correo='+str,
			function(data){
				$('#buscar').show();
				if(data==1){
					rojo(2);
					$('#guardado2').html('Este Correo ya existe');
					$('#btnaceptar').linkbutton('disable');
					gloarra[0]=1
				}else{
					verde(2);
					$('#guardado2').html('disponible');
					$('#btnaceptar').linkbutton('enable');
					gloarra[0]=4
				}
				if(gloarra[0]==1 || gloarra[1]==1 )
				{ 
					if(gloarra[0]==1){rojo(2);
					$('#guardado2').html('Este Correo ya existe');
					$('#btnaceptar').linkbutton('disable');}
					if(gloarra[1]==1){rojo(2);
					$('#guardado2').html('Esta Cedula ya existe');
					$('#btnaceptar').linkbutton('disable');}
				}
				if(gloarra[0]==4 && gloarra[1]==4 )
				{
					$('#guardado2').html('Datos correctos');
					$('#btnaceptar').linkbutton('enable');

				}
			});
		}

	</script>
<?php include '../home/head_2.php'; ?>

        <div id="main" name="main" data-options="region:'center'">
            <table id="dg" title="Usuario" width="100%" align="center" class="easyui-datagrid"
                url="get.php" fit="true"
                toolbar="#toolbar" pagination="true" striped="true"
                rownumbers="false" fitColumns="true" singleSelect="true"
                data-options="
					    fit:true,
					    nowrap:false,
					    singleSelect:true,
					    fitColumns:true
    				"                             
                >
            <thead>
                <tr>
                    <th field="nombre" width="20%" sortable="true">Nombre</th>
                    <th field="apellido" width="20%" sortable="true">Apellido</th>
                    <th field="cedula" width="10%" sortable="true">C&eacute;dula</th><th field="email" width="20%" sortable="true">Email</th>
                    <th field="celular" width="10%" sortable="true">Celular</th>
                    <th field="permiso" width="20%" sortable="true">Permisologia</th>
                        
                </tr>
            </thead>
        </table>
        
	<div style="display:none">
        <div id="toolbar">
            <a class="easyui-linkbutton" iconCls="icon-newdoc" plain="true" onclick="newRecord()">Nuevo</a>
            
            
            <a class="easyui-linkbutton" iconCls="icon-modify" plain="true" onclick="editRecord()">Modificar</a>
            
            
             <a class="easyui-linkbutton" iconCls="icon-erase" plain="true" onclick="removeRecord()">Eliminar</a> 
            <div id="busqueda" style="float:right">
            <input id="criteria" name="criteria" style="width:400px" data-options="
                    prompt: 'escriba la informaci&oacute;n a buscar...',
                    icons:[{
                        iconCls:'icon-search',
                        handler: function(e){

                            $('#dg').datagrid('load',
                            {
                                criteria:$('#criteria').textbox('getValue')
                            });
                          
                        }
                    }]
                    ">
            <script>
                $.extend($.fn.textbox.methods, {
                    addClearBtn: function(jq, iconCls){
                        return jq.each(function(){
                            var t = $(this);
                            var opts = t.textbox('options');
                            opts.icons = opts.icons || [];
                            opts.icons.unshift({
                                iconCls: iconCls,
                                handler: function(e){
                                    $(e.data.target).textbox('clear').textbox('textbox').focus();
                                    $(this).css('visibility','hidden');
									
									$('#dg').datagrid('load',
									{
										criteria:$('#criteria').textbox('getValue')
									});
                                }
                            });
                            t.textbox();
                            if (!t.textbox('getText')){
                                t.textbox('getIcon',0).css('visibility','hidden');
                            }
                            t.textbox('textbox').bind('keyup', function(){
                                var icon = t.textbox('getIcon',0);
                                if ($(this).val()){
                                    icon.css('visibility','visible');
                                } else {
                                    icon.css('visibility','hidden');
                               }
                            });
                        });
                    }
                });
                
                $(function(){
                    $('#criteria').textbox().textbox('addClearBtn', 'icon-clear');
                });
            </script>
            </div>
        </div>
        
        <div id="dlg" class="easyui-dialog" style="width:600px;height:600px;padding:10px 20px; position:relative;" closed="true" modal="true" buttons="#dlg-buttons">
            <div class="ftitle">Informaci&oacute;n del Usuario</div>
            <form id="fm" method="post" novalidate autocomplete="off">
            	<div class="fitem">
                    <label>Nombre</label>
                    <input id="nombre" name="nombre" class="easyui-textbox" style="height:20px; width:300px" required="true">
                </div>
                      	<div class="fitem">
                    <label>Apellido</label>
                    <input id="apellido" name="apellido" class="easyui-textbox" style="height:20px; width:300px" required="true">
                </div>
                      	<div class="fitem">
                    <label>C&eacute;dula</label>                
                    <input id="cedula" name="cedula" class="easyui-numberbox"  style="height:20px; width:300px" data-options="required:true,validType:'length[6,8]'">
                </div>
               <div class="fitem">
                    <label>Correo electr&oacute;nico</label>
                    <input name="correo" id="correo" type="text" class="easyui-validatebox textbox"  data-options="validType:'email'" required="true" style="height:20px; width:300px;text-transform:lowercase;">
                 	<div id="buscar" style="color:#FF0000; width:200px; text-align:right; font-size:12px; font-weight:bold; display:none;"></div>
					<script>
						$('#correo').textbox().textbox('textbox').bind('blur',function(e){
                        	buscarCodigo();
						});						
                    </script>
               </div>      
                   
                      	<div class="fitem">
                    <label>Contraseña</label>
                    <input type="password" id="contrasena" name="contrasena" class="easyui-textbox" style="height:20px; width:300px" required="true">
                </div>
               
                <div class="fitem">
                    <label>Telefono trabajo</label>
                    <input id="telefono" name="telefono" class="easyui-numberbox" maxlength="11" style="height:20px; width:300px" >
                </div>
                      	<div class="fitem">
                    <label>Celular</label>
                    <input id="celular" name="celular" class="easyui-numberbox" onkeypress="key()" maxlength="11" style="height:20px; width:300px">
                </div>
 
       
        <div class="fitem">
        
                 
                 <label>Permisologia</label>
                    <input class="easyui-combobox" 
                    id="permiso"
                    name="permiso"
                    style="width:300px;"
                    required="true"
                    editable="false"
                    data-options="
                        url:'load_permiso.php',
                        valueField:'id',
                        textField:'description',
                        panelHeight:'auto'
                    ">
                    
                </div>
               
              
                </form>
            <div id="guardado2" align="center" style="border-style:solid; font:Verdana, Geneva, sans-serif; border-width:1px; border-color:#00FF00; background-color:#F0FFF0; bottom:5px; width:92%; z-index:0; position:absolute; border-spacing:inherit; font-size:14px; display:none; padding:5px;"></div>
        </div>
        <div id="dlg-buttons">
            <a id="btnaceptar" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveRecord()">Guardar</a>
            <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Cancelar</a>
        </div>
        <div id="pie"><?php// include("../home/pie.php"); ?></div>
        	</div>
        </div>
    </div>
</body>
</html>