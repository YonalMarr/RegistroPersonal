<?php
	include '../home/user_validate.php';
	
	$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
	
    $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'auth_user.id';  
    $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC'; 
	
    $criteria=isset($_POST['criteria']) ? pg_escape_string($_POST['criteria']) : '';
	
	$offset = ($page-1)*$rows;
	
	$result = array();

	if($criteria !== ""){
		$where = " 
		(
		auth_user.first_name  LIKE '%".$criteria."%' OR
		auth_user.last_name  LIKE '%".$criteria."%' OR
		auth_user.email  LIKE '%".$criteria."%' OR
		auth_user.cedula  LIKE '%".$criteria."%' OR
		auth_user.mobilenumber  LIKE '%".$criteria."%'  
		
		)  AND  auth_user.id > 0";
	}
	else{
		$where = " auth_user.id > 0";
	}
	
	$rs = pg_query("
	SELECT 
	auth_user.first_name as nombre,
	auth_user.last_name as apellido,
	auth_user.email as email,
	auth_user.cedula as cedula,
	auth_user.tel_trab as telefono,
	auth_user.mobilenumber as celular,
	auth_user.password,
	auth_group.description as permiso,
	auth_user.id
	FROM
	auth_user
	left Join auth_membership ON auth_membership.user_id = auth_user.id
   left Join auth_group ON auth_membership.group_id = auth_group.id 
	WHERE ".$where);
	
	$row = pg_fetch_row($rs);
	
	$result["total"] = pg_num_rows($rs);
	
	$rs = pg_query("
	SELECT 
	auth_user.first_name as nombre,
	auth_user.last_name as apellido,
	auth_user.email as email,
	auth_user.cedula as cedula,
	auth_user.tel_trab as telefono,
	auth_user.mobilenumber as celular,
	auth_user.password,
	auth_group.description as permiso,
	auth_user.id
	FROM
	auth_user

	left Join auth_membership ON auth_membership.user_id = auth_user.id
   left Join auth_group ON auth_membership.group_id = auth_group.id 
	WHERE  ".$where." and auth_user.email != 'admin' 
	ORDER BY $sort $order LIMIT $rows OFFSET $offset");
	
	$items = array();
	while ($row = pg_fetch_assoc($rs)) {
		
		$row['nombre']=strtoupper(utf8_decode($row['nombre']));
		$row['apellido']=strtoupper(utf8_decode($row['apellido']));
		
		$items[] = array_map('utf8_encode', $row);
	}	
	$result["rows"] = $items;
	
	echo json_encode($result);
?>