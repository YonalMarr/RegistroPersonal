<?php
header('Content-Type: text/html; charset=utf-8');

	include '../home/user_validate.php';
	
	$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
	$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
    $sort = isset($_POST['sort']) ? strval($_POST['sort']):'a.id';
    $order = isset($_POST['order']) ? strval($_POST['order']) : 'ASC'; 
    $criteria=isset($_POST['criteria']) ? pg_escape_string($_POST['criteria']) : '';
	
	$offset = ($page-1)*$rows;
	
	$result = array();

	if($criteria !== ""){
		$where = " 
		(
		a.nombres  LIKE '%".$criteria."%' OR
		a.apellidos  LIKE '%".$criteria."%' OR
		a.correo  LIKE '%".$criteria."%' OR
		a.cedula  LIKE '%".$criteria."%' OR
		b.descripcion  LIKE '%".$criteria."%'		 
		)  AND  a.id > 0";
	}
	else{
		$where = " a.id > 0";
	}
	
	$rs = pg_query("
	SELECT 
	(nombres || ' ' || apellidos) as nombre_completo,
	*
	FROM
	l_base_personal 
	WHERE ".$where);
	
	$row = pg_fetch_row($rs);
	
	$result["total"] = pg_num_rows($rs);
	
	$rs = pg_query("
	SELECT 
		(a.nombres || ' ' || a.apellidos) as nombre_completo
		,a.nombres
		,a.apellidos
		,a.id
		,a.cedula
		,a.correo
		,a.telefono
		,a.cargo
		,a.horario
		,b.descripcion
		,a.status
		,a.dpto
		,a.fecha_nacimiento
		,a.fecha_lectura
		,a.fuente
		,a.direccion
	FROM
	l_base_personal a,
	l_status b
	WHERE  a.status = b.id and ".$where." 

	ORDER BY $sort $order LIMIT $rows OFFSET $offset");
	
	$items = array();
	while ($row = pg_fetch_assoc($rs)) {
		
		$row['nombre']=mb_strtoupper($row['nombre']);
		$row['apellido']=mb_strtoupper($row['apellido']);
		
		$items[] = $row;
	}
	$result["rows"] = $items;
	
	echo json_encode($result);
?>