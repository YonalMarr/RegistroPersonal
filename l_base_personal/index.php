<?php include '../home/head.php'; ?>

<script type="text/javascript">
  var url;
  var read_noly;
  var esta_global;
  var id;
  
  function newRecord(){
    $('#dlg').dialog('open').dialog('setTitle','Nuevo');
   	$('#fm').form('clear');
   	$('#btnaceptar').linkbutton('enable');
    $('#guardado2').hide();			
   	read_noly = 0;
   	esta_global = 0;
  }
  
  function editRecord(){
    var rows = $('#dg').datagrid('getSelections');
    if(rows!=''){
      if(rows.length != 1){
        swal('Debe seleccionar una (1) persona');
        return false;
      }else{
        var row = rows[0];
        $('#dlg').dialog('open').dialog('setTitle','Modificar');
        esta_global = 1;
        $('#buscar').html('');
        $('#btnaceptar').linkbutton('enable');
        $('#fm').form('load',row);
        id = row.id;
      }
    }else{
      swal('Debe seleccionar un (1) persona');
      return false;
    }
    read_noly = 1;
    esta_global = 1;
  }
  		
  function saveRecord(){
    if($('#nombres').textbox('isValid') == false){
   	  rojo(2);
   		$('#guardado2').html('Ingrese el nombres del Usuario');
   		$('#nombre').textbox('textbox').focus();
   		return false;
   	}
   	if($('#apellidos').textbox('isValid') == false){
   		rojo(2);
   		$('#guardado2').html('Ingrese el apellido del usuario');
   		$('#apellido').textbox('textbox').focus();
   		return false;
   	}
   	if($('#cedula').numberbox('isValid') == false){
   		rojo(2);
   		$('#guardado2').html('Ingrese cedula del Usuario');
   		$('#cedula').numberbox('textbox').focus();
   		return false;
   	}
		if($('#correo').validatebox('isValid') == false){
 			rojo(2);
      $('#guardado2').html('Ingrese el correo electr&oacute;nico');
   		$('#correo').textbox('textbox').focus();
   		return false;
   	}			
    if(esta_global == 1){				
   	  $('#fm').form('submit',{
        url: 'update.php?id='+id,
        success: function(result){
          var result = eval('('+result+')');
   				if (result.success){
   			    $('#dlg').dialog('close');		
            $('#dg').datagrid('reload');	
   				} else {
   			    $.messager.show({
   			      title: 'Error',
   				 		msg: result.msg
   					});
   				}
   			}
   		});
   		verde(2);
   		$('#guardado2').html('El registro ha sido modificado satisfactoriamente');
   	}else{
   	  $('#fm').form('submit',{
   	    url: 'save.php',
   			success: function(result){
   			  var result = eval('('+result+')');
   				if (result.success){
   					$('#dlg').dialog('close');		// close the dialog
   					$('#dg').datagrid('reload');	// reload the user data
   				} else {
   			    $.messager.show({
   					  title: 'Error',
   						msg: result.msg
   					});
   				}
   			}
   		});
   	}
  }


  function verde(num){
  	$('#guardado'+num).css('font-size','14px');
    $('#guardado'+num).css('padding','5px');
   	$('#guardado'+num).css('border-color','#00FF00');
   	$('#guardado'+num).css('background-color','#F0FFF0');
   	$('#guardado'+num).css('display','block');
   	$("#guardado"+num).fadeOut(4000);
  }
  		
  function rojo(num){
    $('#guardado'+num).css('font-size','14px');
   	$('#guardado'+num).css('padding','5px');
   	$('#guardado'+num).css('border-color','#FF0000');
   	$('#guardado'+num).css('background-color','#FDE9D9');
   	$('#guardado'+num).css('display','block');
   	$("#guardado"+num).fadeOut(3000);
  }

  function enviarCorreo(){
    var ss = [];
    var rows = $('#dg').datagrid('getSelections');
    var total = rows.length;
    var max = 0;
    var stdif = 0;

    if(rows!=''){
      $.messager.prompt('Fecha', 'Inserte la fecha (dd-mm-aaaa):', function(fecha_entrevista){
        if (fecha_entrevista){
          $.messager.prompt('Hora', 'Inserte la hora de la entrevista (hh:mm xx):', function(hora){
              if (hora){
                for(var i=0; i<rows.length; i++){
                  var row = rows[i];
                  if(row.status == 0){
                    var parametros = {
                      "id":row.id,
                      "correo": row.correo,
                      "nombres":row.nombres,
                      "apellidos":row.apellidos,
                      "cedula":row.cedula,
                      "hora":hora,
                      "fecha":fecha_entrevista
                    }
                    var parametros_2 = {
                      "id":row.id,
                      "hora":hora,
                      "fecha":fecha_entrevista
                    }
                    $.ajax({
                      data:  parametros,
                      url:   'mail.php',
                      type:  'post',   
                      success:  function (data) { }
                    });
                    $.ajax({
                      data:  parametros_2,
                      url:   'status.php',
                      type:  'post',   
                      success:  function (data) { }
                    });              
                  }
                }
                max = 1 + i;
                if(i == total){
                  $('#dg').datagrid('reload');
                }
              }
          });  
        }
      });
    }else{
      swal('Debe seleccionar por lo menos una (1) persona');
      return false;
    }
  }
  </script>
  <?php include '../home/head_2.php'; ?>
  <div id="main" name="main" data-options="region:'center'">
  <table id="dg" 
            title="Base General" 
            width="100%" 
            align="center" 
            class="easyui-datagrid" 
            url="get.php" 
            fit="true" 
            toolbar="#toolbar" 
            pagination="true" 
            striped="true" 
            rownumbers="false" 
            fitColumns="true" 
            singleSelect="false"
             autoRowHeight="false" 
            data-options="fit:true, 
                           nowrap:false, 
                           fitColumns:true, 
                           rowStyler: function(index,row){
                              if (row.status == 1){
                              return 'background-color:#6293BB;color:#fff;font-weight:bold;';
                              }else if(row.status == 9){
                              return 'background-color:#FD4646;color:#fff;font-weight:bold;';
                              }else if (row.status == 2){
                              return 'background-color:#61FF86;color:#000;font-weight:bold;';
            }
                           }"
            >
               <thead data-options="frozen:true">
                  <tr>
                     <th data-options="field:'ck',checkbox:true"></th>
                     <th field="nombre_completo" width="25%" sortable="true">Nombre</th>
                     <th field="cedula" width="10%" sortable="true">C&eacute;dula</th> 
                     <th field="correo" width="20%" sortable="true">Email</th>
                     <th field="telefono" width="12%" sortable="false">Celular</th>
                  </tr>
               </thead>
               <thead>
                  <tr>           
                     <th field="descripcion" width="30%" sortable="true">Estatus</th> 
                     <th field="cargo" width="20%" sortable="true">Cargo</th> 
                     <th field="horario" width="20%" sortable="true">Horario</th>             
                  </tr>
               </thead>
           </table>
         <div style="display:none">
            <div id="toolbar">
               <a class="easyui-linkbutton" iconCls="icon-newdoc" plain="true" onclick="newRecord()">Nuevo</a>
               <a class="easyui-linkbutton" iconCls="icon-modify" plain="true" onclick="editRecord()">Modificar</a>
<!--<a class="easyui-linkbutton" iconCls="icon-erase" plain="true" ="removeRecord()">Eliminar</a> -->
               <a class="easyui-linkbutton" iconCls="icon-mail" plain="true" onclick="enviarCorreo()">Enviar a Entrevista</a>
               <div id="busqueda" style="float:right">
                  <input id="criteria" name="criteria" style="width:400px" data-options="
                     prompt: 'escriba la informaci&oacute;n a buscar...',
                     icons:[{
                     iconCls:'icon-search',
                     handler: function(e){
                        $('#dg').datagrid('load',
                           {criteria:$('#criteria').textbox('getValue')}
                           );
                        }
                     }]">
               <script>
                   $.extend($.fn.textbox.methods, {
                       addClearBtn: function(jq, iconCls){
                           return jq.each(function(){
                               var t = $(this);
                               var opts = t.textbox('options');
                               opts.icons = opts.icons || [];
                               opts.icons.unshift({
                                   iconCls: iconCls,
                                   handler: function(e){
                                       $(e.data.target).textbox('clear').textbox('textbox').focus();
                                       $(this).css('visibility','hidden');
                              
                              $('#dg').datagrid('load',
                              {
                                 criteria:$('#criteria').textbox('getValue')
                              });
                                   }
                               });
                               t.textbox();
                               if (!t.textbox('getText')){
                                   t.textbox('getIcon',0).css('visibility','hidden');
                               }
                               t.textbox('textbox').bind('keyup', function(){
                                   var icon = t.textbox('getIcon',0);
                                   if ($(this).val()){
                                       icon.css('visibility','visible');
                                   } else {
                                       icon.css('visibility','hidden');
                                  }
                               });
                           });
                       }
                   });
                   
                   $(function(){
                       $('#criteria').textbox().textbox('addClearBtn', 'icon-clear');
                   });
               </script>            

               </div>
           </div>
           
           <div id="dlg" class="easyui-dialog" style="width:600px;height:600px;padding:10px 20px; position:relative;" closed="true" modal="true" buttons="#dlg-buttons">
               <div class="ftitle">Registro de Personal</div>
               <form id="fm" method="post" novalidate autocomplete="off">
                   <div class="fitem">
                       <label>C&eacute;dula</label>                
                       <input id="cedula" name="cedula" class="easyui-numberbox"  style="height:20px; width:300px" data-options="	required:true,validType:'length[6,8]',
                               valueField:'cedula',
                               onChange: function(re){
                               if(esta_global!=1){
                               	$.post('find.php?cedula='+re,
    									function(data){
    										if(data==1){
    											rojo(2);
   				      					$('#guardado2').html('La Cedula Ya Existe');
   				      					$('#btnaceptar').linkbutton('disable');
   										}else{
   										$('#btnaceptar').linkbutton('enable');


   										}
   									});}else{
   										$('#btnaceptar').linkbutton('enable');
   									}	
    								}">

                   </div>
               	<div class="fitem">
                       <label>Nombre</label>
                       <input id="nombres" name="nombres" class="easyui-textbox" style="height:20px; width:300px" required="true">
                   </div>
                    <div class="fitem">
                       <label>Apellido</label>
                       <input id="apellidos" name="apellidos" class="easyui-textbox" style="height:20px; width:300px" required="true">
                   </div>

                  <div class="fitem">
                       <label>Correo electr&oacute;nico</label>
                       <input name="correo" id="correo" type="text" class="easyui-validatebox textbox"  data-options="validType:'email'" required="true" style="height:20px; width:300px;text-transform:lowercase;">
                    	<div id="buscar" style="color:#FF0000; width:200px; text-align:right; font-size:12px; font-weight:bold; display:none;"></div>

                  </div>      
                      

                   <div class="fitem">
                       <label>Celular</label>
                       <input id="telefono" name="telefono" class="easyui-numberbox"  maxlength="11" style="height:20px; width:300px"  required="true">
                   </div>
          			<div class="fitem">
               		<label>Fecha de Nacimiento</label>
               		<input id="fecha_nacimiento" name="fecha_nacimiento" class="easyui-datebox" style="width:125px;" data-options="formatter:myformatter,parser:myparser" required="true"></input>
               <script type="text/javascript">
                   function myformatter(date){
                       var y = date.getFullYear();
                       var m = date.getMonth()+1;
                       var d = date.getDate();
                       return (d<10?('0'+d):d)+'-'+(m<10?('0'+m):m)+'-'+y;
                   }
                   function myparser(s){
                       if (!s) return new Date();
                       var ss = s.split('-');
                       var y = parseInt(ss[0],10);
                       var m = parseInt(ss[1],10);
                       var d = parseInt(ss[2],10);
                       if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
                           return new Date(d,m-1,y);
                       } else {
                           return new Date();
                       }
                   }		$('#fecha_nacimiento').datebox({onSelect: function(date){validar_fechas();
   			}
   		});	

               </script><label><font color="#FFB8B8">dd-mm-aaaa</font></label>
                   </div>
                   
                   <div class="fitem">
   				<label>Direcci&oacute;n</label>
                 <input class="easyui-textbox" id="direccion" name="direccion" multiline="true" style="width:300px;height:50px">
   				</div>
                   <br><br>
               <div class="ftitle">Proceso inicial de selecci&oacute;n</div>
   			<div class="fitem">
               		<label>Lectura de CV</label>
               		<input id="fecha_lectura" name="fecha_lectura" class="easyui-datebox" style="width:125px;" data-options="formatter:myformatter,parser:myparser" required="true"></input>
               <script type="text/javascript">
                   function myformatter(date){
                       var y = date.getFullYear();
                       var m = date.getMonth()+1;
                       var d = date.getDate();
                       return (d<10?('0'+d):d)+'-'+(m<10?('0'+m):m)+'-'+y;
                   }
                   function myparser(s){
                       if (!s) return new Date();
                       var ss = s.split('-');
                       var y = parseInt(ss[0],10);
                       var m = parseInt(ss[1],10);
                       var d = parseInt(ss[2],10);
                       if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
                           return new Date(d,m-1,y);
                       } else {
                           return new Date();
                       }
                   }		$('#fecha_lectura').datebox({onSelect: function(date){validar_fechas();
   			}
   		});	

               </script><label><font color="#FFB8B8">dd-mm-aaaa</font></label>
                   </div>

                   <div class="fitem">
                       <label>Fuente de lectura</label>
                       <input id="fuente" name="fuente" class="easyui-combobox" style="height:20px; width:300px"  required="true" editable="false"
                       data-options="
                            url:'load_captacion.php',
                            valueField:'id',
                            textField:'descripcion',
                            panelHeight:'auto',
                           onSelect: function(rec3){ }
                    ">
                   </div>

                   <div class="fitem">
                       <label>Departamento</label>
                       <input id="dpto" name="dpto" class="easyui-combobox"  style="height:20px; width:300px" editable="false"
                       data-options="
                            url:'load_depto.php',
                            valueField:'id',
                            textField:'descripcion',
                            panelHeight:'auto',
                            onSelect: function(rec3){ }
                    ">
                   </div>

                   <div class="fitem">
                       <label>Cargo</label>
                       <input id="cargo" name="cargo" class="easyui-combobox" style="height:20px; width:300px" editable="false"
                       data-options="
                            url:'load_cargo.php',
                            valueField:'id',
                            textField:'descripcion',
                            panelHeight:'auto',
                            onSelect: function(rec3){ }
                    ">
                   </div>

                   <div class="fitem">
                       <label>Horario</label>
                       <input id="horario" name="horario" class="easyui-combobox" style="height:20px; width:300px" editable="false"
                       data-options="
                            url:'load_horario.php',
                            valueField:'id',
                            textField:'descripcion',
                            panelHeight:'auto',
                            onSelect: function(rec3){ }
                    ">
                   </div>
                   </form>
   <div id="guardado2" align="center" style="border-style:solid; font:Verdana, Geneva, sans-serif; border-width:1px; border-color:#00FF00; background-color:#F0FFF0; bottom:5px; width:92%; z-index:0; position:absolute; border-spacing:inherit; font-size:14px; display:none; padding:5px;"></div>
           </div>
           <div id="dlg-buttons">
               <a id="btnaceptar" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveRecord()">Guardar</a>
               <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Cancelar</a>
           </div>
          <div id="pie">
            <?php include("../home/pie.php"); ?>
            </div>
          </div>
           </div>
       </div>
   </body>
   </html>