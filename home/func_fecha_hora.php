<?php 
	#GENERA LA FECHA Y HORA ACTUAL Y POSEE LAS FUNCIONES PARA VOLTEO DE FECHA
	ini_set('date.timezone','America/Caracas'); 
	$fecha=date("Y/m/d");
	$hora=date("h:i:s");
	
	function implota($fecha) {// CONVIERTE LA FECHA DE LA BASE DE DATOS (Y-M-D) A FECHA LOCAL (D/M/Y)
		if (($fecha == "") || ($fecha == "0000-00-00"))
		return "";
		$vector_fecha = explode("-",$fecha);
		$aux = $vector_fecha[2];
		$vector_fecha[2] = $vector_fecha[0];
		$vector_fecha[0] = $aux;
		return implode("/",$vector_fecha);
	}

	function explota($fecha){ // CONVIERTE LA FECHA LOCAL (D/M/Y) A LA DE BASE DE DATOS (Y-M-D)
		$vector_fecha = explode("/",$fecha);
		$aux = $vector_fecha[2];
		$vector_fecha[2] = $vector_fecha[0];
		$vector_fecha[0] = $aux;
		return implode("-",$vector_fecha);
	};
?>