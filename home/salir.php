<?php include '../home/head.php'; ?>

	<style>
        about_title{
            color:#534F4F;
            font-family:Arial, Helvetica, Verdana;
            font-size:19px;
            font-weight:bold;
            text-align:center;
        }
        author{
            color:#003399;
            font-family:Arial, Helvetica, Verdana;
            font-size:15px;
            font-weight:bold;
            text-align:center;
        }
        author_email{
            color:#000066;
            font-family:Arial, Helvetica, Verdana;
            font-size:11px;
            text-align:center;
        }
        free_msg{
            color:#446D8C;
            font-family:Arial, Helvetica, Verdana;
            font-size:17px;
            font-weight:bold;
            text-align:center;
        }
        copy_right{
            color:#000066;
            font-family:Arial, Helvetica, Verdana;
            font-size:9px;
            text-align:center;
        }
        html	{overflow: hidden; height: 100%;}
        body	{overflow: auto; height: 100%; width: 90%; margin: 0 auto; /* center */ padding: 10px 20px; border-width: 0 1px;}
    </style>
</head>
<body>
    <div class="easyui-layout" style="width:100%; height:100%;">
        <div data-options="region:'north',border:false" style="height:110px;padding:10px 0 10px 0;">
        	<?php include("../home/encabezado.php"); ?>
            <div id="titulo"><?php include("../home/title.php"); ?></div>
        </div>

        <div data-options="region:'south',border:false" style="height:50px;padding:10px;"><?php include("../home/pie.php"); ?></div>
        <div data-options="region:'center'"></div>
    </div>
    <div id="w" class="easyui-window" title="Saliendo del sistema..." data-options="modal:true,closed:false,minimizable:false,maximizable:false,collapsible:false,closable:false,resizable:false,draggable:false" style="width:450px;height:375px;padding:10px;">
        <center><about_title><?php include 'title.php'; ?></about_title></center>
        <br/>
        <center><img src="../src/img/logo.png" width="78" height="110"/></center>
        <br/>
        <center><free_msg>Gracias por utilizar este recurso !!!</free_msg></center>
        <br/>
        <br/>
        <center><a onclick="linkPage('../site/')" style="width:120px" class="easyui-linkbutton c7">Salir</a></center>
        <br/>
        <br/>
        <center><copy_right><?php include("pie.php"); ?></copy_right></center>
    </div>
</body>
</html>