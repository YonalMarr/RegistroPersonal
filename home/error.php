<?php 
	include '../home/title_define.php';
	include '../home/icon_define.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo "$page_tittle"; ?></title>
    <link rel="shortcut icon" href="<?php echo "$page_icon"; ?>">
	<link rel="stylesheet" type="text/css" href="../src/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../src/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../src/themes/demo.css">
	<link rel="stylesheet" type="text/css" href="../src/themes/color.css">
	<script type="text/javascript" src="../src/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="../src/jquery/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../src/jquery/easyui-lang-es.js"></script>
	<style>
        about_title{
            color:#534F4F;
            font-family:Arial, Helvetica, Verdana;
            font-size:19px;
            font-weight:bold;
            text-align:center;
        }
        err_title{
            color:#FF0000;
            font-family:Arial, Helvetica, Verdana;
            font-size:12px;
            font-weight:bold;
            text-align:center;
        }
        err_explanation{
            color:#000066;
            font-family:Arial, Helvetica, Verdana;
            font-size:12px;
            text-align:center;
        }
        error_msg{
            color:#FF0000;
            font-family:Arial, Helvetica, Verdana;
            font-size:22px;
            font-weight:bold;
            text-align:center;
        }
        copy_right{
            color:#000066;
            font-family:Arial, Helvetica, Verdana;
            font-size:9px;
            text-align:center;
        }
        html	{overflow: hidden; height: 100%;}
        body	{overflow: auto; height: 100%; width: 90%; margin: 0 auto; /* center */ padding: 10px 20px; border-width: 0 1px;}
    </style>
</head>
<body>
    <div class="easyui-layout" style="width:100%; height:100%;">
        <div data-options="region:'north',border:false" style="height:110px;padding:10px 0 10px 0;">
        	<?php include("../home/encabezado.php"); ?>
            <div id="titulo"><?php include("../home/title.php"); ?></div>
        </div>

        <div data-options="region:'south',border:false" style="height:50px;padding:10px;"><?php include("../home/pie.php"); ?></div>
        <div data-options="region:'center'"></div>
    </div>

    <div id="w" class="easyui-window" title="Error" data-options="modal:true,closed:false,minimizable:false,maximizable:false,collapsible:false,closable:false,resizable:false,draggable:false" style="width:450px;height:375px;padding:10px;">
    <center><about_title><?php include '../home/title.php'; ?></about_title></center>
    <br/>
    <center><img src="../src/img/logo.png" width="150" height="110"/></center>
    <br/>
    <center><err_title>Ha tratado de ingresar al sistema sin registrarse o no ha habido actividad desde hace 1440 o más segundos</err_title></center>
    <br/>
    <center><err_explanation>Seleccione la página de acceso e ingrese correctamente su nombre de usuario y clave</err_explanation></center>
    <br/>
    <br/>
    <center><a onclick="linkPage('../site/')" style="width:120px" class="easyui-linkbutton c6">Aceptar</a></center>
    </div>
</body>
</html>