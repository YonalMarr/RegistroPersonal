  <style>
           html   {overflow: hidden; height: 100%;}
           body   {overflow: auto; height: 100%; width: 90%; margin: 0 auto; /* center */ padding: 10px 20px; border-width: 0 1px;}
  </style>
  </head>
  <body>
   <div class="easyui-layout" style="width:100%; height:100%;">
      <div data-options="region:'north',border:false" style="height:110px;padding:10px 0 10px 0;">
         <?php include("../home/encabezado.php"); ?>
         <div id="titulo"><?php include("../home/title.php"); ?></div>
            <div id="user_name">
               <?php include("../home/user_tipo_user.php"); ?>
            </div>
         </div>
         <div data-options="region:'west',split:true,title:'Procesos'" style="width:20%;text-align:center;">
            <?php include("../home/menu_west.php"); ?>
         </div>
         <div data-options="region:'south',border:false" style="height:50px;padding:10px;">
            <?php include("../home/pie.php"); ?>
         </div>