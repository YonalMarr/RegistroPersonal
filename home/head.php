<?php 
    header('Content-Type: text/html; charset=utf-8');
   	include '../home/user_validate.php';
   	include '../home/title_define.php';
   	include '../home/icon_define.php';
?>

   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
   <html xmlns="http://www.w3.org/1999/xhtml">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <head>
    <title><?php echo "$page_tittle"; ?></title>
    <link rel="shortcut icon" href="<?php echo "$page_icon"; ?>">
	<link rel="stylesheet" type="text/css" href="../src/themes/material/easyui.css">
	<link rel="stylesheet" type="text/css" href="../src/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../src/themes/demo.css">
	<link rel="stylesheet" type="text/css" href="../src/themes/color.css">
	<script type="text/javascript" src="../src/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="../src/jquery/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../src/jquery/easyui-lang-es.js"></script>
   <script src="../src/jquery/sweetalert.min.js"></script> 
   <link rel="stylesheet" type="text/css" href="../src/themes/sweetalert.css">
	<style type="text/css">
		#fm{
   			margin:0;
   			padding:10px 30px;
   		}
   		.ftitle{
   			font-size:14px;
   			font-weight:bold;
   			color:#666;
   			padding:5px 0;
   			margin-bottom:10px;
   			border-bottom:1px solid #ccc;
   		}
   		.fitem{
   			margin-bottom:5px;
   		}
   		.fitem label{
   			display:inline-block;
   			width:120px;
   		}
   		#buscar{
   			display:inline-block;
   			width:190px;
   			float:right;
   		}
	</style>