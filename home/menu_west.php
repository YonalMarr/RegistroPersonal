<div class="easyui-accordion" style="width:100%;height:100%;border:0;overflow:auto">		


<!--******************************SUPERADMIN******************************-->
<?php
if(ucwords($tipo_usuario_global)!= 'Superadmin') {  
?>  
<br/>
<a onclick="linkPage('../site/home.php')" class="easyui-linkbutton c7" data-options="plain:true" style="width:90%"><strong>Inicio</strong></a>
<br/> 
<br/><br/>
<a onclick="linkPage('../l_base_personal/')" class="easyui-linkbutton c7" data-options="plain:true" style="width:90%"><strong>Base</strong></a>
<br/>
<br/>
<a onclick="linkPage('../l_entrevistas/')" class="easyui-linkbutton c7" data-options="plain:true" style="width:90%"><strong>Entrevistas</strong></a>
<br/>
<br/>
<a onclick="linkPage('../l_exam_med/')" class="easyui-linkbutton c7" data-options="plain:true" style="width:90%"><strong>Examenes M&eacute;dicos / VPC</strong></a>
<br/>
<br/>
<a onclick="linkPage('../l_capacitacion/')" class="easyui-linkbutton c7" data-options="plain:true" style="width:90%"><strong>Capacitaci&oacute;n</strong></a>
<br/><br/>
<br/><br/>
<a class="easyui-linkbutton c7" onclick="$('#about').dialog('open')" data-options="plain:true" style="width:90%"><strong>Acerca de</strong></a>
<br/><br/>
<a class="easyui-linkbutton c7" onclick="linkPage('../home/salir.php')" data-options="plain:true" style="width:90%"><strong>Salir</strong></a>

<?php
}
########################################################################################
if(ucwords($tipo_usuario_global)== 'Superadmin') {  
?>   
<br/>
<a onclick="linkPage('../site/home.php')" class="easyui-linkbutton c5" data-options="plain:true" style="width:90%"><strong>Inicio</strong></a>
<br/>
<br/><br/>
<a onclick="linkPage('../c_usuario/')" class="easyui-linkbutton c5" data-options="plain:true" style="width:90%">Usuarios</a>
<br/><br/> 
<a onclick="linkPage('../c_roles/')" class="easyui-linkbutton c5" data-options="plain:true" style="width:90%">Roles de usuarios</a>

<br/><br/><br/><br/> 

<a onclick="linkPage('../c_captacion/')" class="easyui-linkbutton c5" data-options="plain:true" style="width:90%">Captaci&oacute;n</a>
<br/><br/> 

<a onclick="linkPage('../c_cargo/')" class="easyui-linkbutton c5" data-options="plain:true" style="width:90%">Cargos</a>
<br/><br/> 

<a onclick="linkPage('../c_depto/')" class="easyui-linkbutton c5" data-options="plain:true" style="width:90%">Departamentos</a>
<br/><br/> 

<a onclick="linkPage('../c_horario/')" class="easyui-linkbutton c5" data-options="plain:true" style="width:90%">Horarios</a>
<br/><br/> 


<br/><br/>
<br/><br/>
<a class="easyui-linkbutton c5" onclick="$('#about').dialog('open')" data-options="plain:true" style="width:90%"><strong>Acerca de</strong></a>
<br/><br/>
<a class="easyui-linkbutton c5" onclick="linkPage('../home/salir.php')" data-options="plain:true" style="width:90%"><strong>Salir</strong></a>


<?php 

}
?>
          
</div>    

	<div style="display:none;">
		<style>
			about_title{
				color:#000066;
				font-family:Arial, Helvetica, Verdana;
				font-size:19px;
				font-weight:bold;
				text-align:center;
			}
			author{
				color:#FF0000;
				font-family:Arial, Helvetica, Verdana;
				font-size:12px;
				font-weight:bold;
				text-align:center;
			}
			author_email{
				color:#B40404;
				font-family:Arial, Helvetica, Verdana;
				font-size:11px;
				text-align:center;
			}
			free_msg{
				color:#446D8C;
				font-family:Arial, Helvetica, Verdana;
				font-size:16px;
				font-weight:bold;
			}
			copy_right{
				color:#000066;
				font-family:Arial, Helvetica, Verdana;
				font-size:9px;
			}
		</style>
	
		<div id="about" class="easyui-dialog" title="Acerca de" data-options="closed:true,modal:true" style="text-align:center;width:450px;height:375px;padding:10px">
			<center><about_title><?php include 'title.php'; ?></about_title>
			<br/>
			<br/>
			<img src="../src/img/logo.png" width="200" height="170"/>
			<br/>
			<br/>
			<author>Yonaldy Marrero</author>
			<br/>
			<author_email>yonalmarr@gmail.com</author_email>
			<br/>
			<br/>
			<free_msg></free_msg></center>
			<br/>
			<center><a onclick="javascript:$('#about').dialog('close')" style="width:120px" class="easyui-linkbutton c7">Aceptar</a></center>
		</div>
</div>
