--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_event; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_event (
    id integer NOT NULL,
    time_stamp timestamp without time zone,
    client_ip character varying(512),
    user_id integer,
    origin character varying(512),
    description text
);


ALTER TABLE auth_event OWNER TO postgres;

--
-- Name: auth_event_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_event_id_seq OWNER TO postgres;

--
-- Name: auth_event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_event_id_seq OWNED BY auth_event.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    role character varying(512),
    description text
);


ALTER TABLE auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_membership; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_membership (
    id integer NOT NULL,
    user_id integer,
    group_id integer
);


ALTER TABLE auth_membership OWNER TO postgres;

--
-- Name: auth_membership_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_membership_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_membership_id_seq OWNER TO postgres;

--
-- Name: auth_membership_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_membership_id_seq OWNED BY auth_membership.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    group_id integer,
    name character varying(512),
    table_name character varying(512),
    record_id integer
);


ALTER TABLE auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    first_name character varying(128),
    last_name character varying(128),
    email character varying(128),
    password character varying(512),
    registration_key character varying(512),
    reset_password_key character varying(512),
    registration_id character varying(512),
    estado_id integer,
    ente_id integer,
    cedula character varying(50),
    tel_hab character varying(50),
    tel_trab character varying(50),
    mobilenumber character varying(50),
    corredor_id integer
);


ALTER TABLE auth_user OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: l_base_personal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE l_base_personal (
    id integer NOT NULL,
    cedula character varying(20),
    nombres character varying(250),
    apellidos character varying(250),
    correo character varying(250),
    telefono character varying(50),
    fecha_nacimiento date,
    direccion text,
    fuente character varying(100),
    dpto character varying(50),
    cargo character varying(150),
    fecha_lectura date,
    horario character varying(150),
    status integer
);


ALTER TABLE l_base_personal OWNER TO postgres;

--
-- Name: l_base_personal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE l_base_personal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE l_base_personal_id_seq OWNER TO postgres;

--
-- Name: l_base_personal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE l_base_personal_id_seq OWNED BY l_base_personal.id;


--
-- Name: l_captacion; Type: TABLE; Schema: public; Owner: jorgerengifo; Tablespace: 
--

CREATE TABLE l_captacion (
    id integer NOT NULL,
    descripcion character varying(150)
);


ALTER TABLE l_captacion OWNER TO jorgerengifo;

--
-- Name: l_captacion_id_seq; Type: SEQUENCE; Schema: public; Owner: jorgerengifo
--

CREATE SEQUENCE l_captacion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE l_captacion_id_seq OWNER TO jorgerengifo;

--
-- Name: l_captacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jorgerengifo
--

ALTER SEQUENCE l_captacion_id_seq OWNED BY l_captacion.id;


--
-- Name: l_cargo; Type: TABLE; Schema: public; Owner: jorgerengifo; Tablespace: 
--

CREATE TABLE l_cargo (
    id integer NOT NULL,
    descripcion character varying(200)
);


ALTER TABLE l_cargo OWNER TO jorgerengifo;

--
-- Name: l_cargo_id_seq; Type: SEQUENCE; Schema: public; Owner: jorgerengifo
--

CREATE SEQUENCE l_cargo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE l_cargo_id_seq OWNER TO jorgerengifo;

--
-- Name: l_cargo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jorgerengifo
--

ALTER SEQUENCE l_cargo_id_seq OWNED BY l_cargo.id;


--
-- Name: l_depto; Type: TABLE; Schema: public; Owner: jorgerengifo; Tablespace: 
--

CREATE TABLE l_depto (
    id integer NOT NULL,
    descripcion character varying(200)
);


ALTER TABLE l_depto OWNER TO jorgerengifo;

--
-- Name: l_depto_id_seq; Type: SEQUENCE; Schema: public; Owner: jorgerengifo
--

CREATE SEQUENCE l_depto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE l_depto_id_seq OWNER TO jorgerengifo;

--
-- Name: l_depto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jorgerengifo
--

ALTER SEQUENCE l_depto_id_seq OWNED BY l_depto.id;


--
-- Name: l_fase_i; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE l_fase_i (
    id integer NOT NULL,
    id_personal integer,
    fecha_entrevista date,
    hora character varying(10),
    activo boolean,
    asistio boolean,
    entr_depto boolean,
    entr_rrhh boolean,
    redaccion boolean,
    ortografia boolean,
    observaciones text
);


ALTER TABLE l_fase_i OWNER TO postgres;

--
-- Name: l_fase_i_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE l_fase_i_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE l_fase_i_id_seq OWNER TO postgres;

--
-- Name: l_fase_i_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE l_fase_i_id_seq OWNED BY l_fase_i.id;


--
-- Name: l_horario; Type: TABLE; Schema: public; Owner: jorgerengifo; Tablespace: 
--

CREATE TABLE l_horario (
    id integer NOT NULL,
    descripcion character varying(200)
);


ALTER TABLE l_horario OWNER TO jorgerengifo;

--
-- Name: l_horario_id_seq; Type: SEQUENCE; Schema: public; Owner: jorgerengifo
--

CREATE SEQUENCE l_horario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE l_horario_id_seq OWNER TO jorgerengifo;

--
-- Name: l_horario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jorgerengifo
--

ALTER SEQUENCE l_horario_id_seq OWNED BY l_horario.id;


--
-- Name: l_status; Type: TABLE; Schema: public; Owner: jorgerengifo; Tablespace: 
--

CREATE TABLE l_status (
    id integer NOT NULL,
    descripcion character varying(150)
);


ALTER TABLE l_status OWNER TO jorgerengifo;

--
-- Name: l_status_id_seq; Type: SEQUENCE; Schema: public; Owner: jorgerengifo
--

CREATE SEQUENCE l_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE l_status_id_seq OWNER TO jorgerengifo;

--
-- Name: l_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jorgerengifo
--

ALTER SEQUENCE l_status_id_seq OWNED BY l_status.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_event ALTER COLUMN id SET DEFAULT nextval('auth_event_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_membership ALTER COLUMN id SET DEFAULT nextval('auth_membership_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY l_base_personal ALTER COLUMN id SET DEFAULT nextval('l_base_personal_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jorgerengifo
--

ALTER TABLE ONLY l_captacion ALTER COLUMN id SET DEFAULT nextval('l_captacion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jorgerengifo
--

ALTER TABLE ONLY l_cargo ALTER COLUMN id SET DEFAULT nextval('l_cargo_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jorgerengifo
--

ALTER TABLE ONLY l_depto ALTER COLUMN id SET DEFAULT nextval('l_depto_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY l_fase_i ALTER COLUMN id SET DEFAULT nextval('l_fase_i_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jorgerengifo
--

ALTER TABLE ONLY l_horario ALTER COLUMN id SET DEFAULT nextval('l_horario_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jorgerengifo
--

ALTER TABLE ONLY l_status ALTER COLUMN id SET DEFAULT nextval('l_status_id_seq'::regclass);


--
-- Data for Name: auth_event; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_event (id, time_stamp, client_ip, user_id, origin, description) FROM stdin;
\.


--
-- Name: auth_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_event_id_seq', 123136, true);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group (id, role, description) FROM stdin;
4	Revisor	Usuario Revisor
99	superadmin	SuperAdministrador
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_id_seq', 21, true);


--
-- Data for Name: auth_membership; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_membership (id, user_id, group_id) FROM stdin;
183	235	99
314	364	4
\.


--
-- Name: auth_membership_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_membership_id_seq', 314, true);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_permission (id, group_id, name, table_name, record_id) FROM stdin;
1	\N	abierto	\N	\N
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_permission_id_seq', 1, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user (id, first_name, last_name, email, password, registration_key, reset_password_key, registration_id, estado_id, ente_id, cedula, tel_hab, tel_trab, mobilenumber, corredor_id) FROM stdin;
364	PRUEBA	PRUEBA	prueba@gmail.com	202cb962ac59075b964b07152d234b70	\N	\N	\N	\N	\N	456456	\N			\N
235	super	admin	admin	202cb962ac59075b964b07152d234b70		\N	\N	1	\N	18557252	\N		4242912828	\N
\.


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_id_seq', 364, true);


--
-- Data for Name: l_base_personal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY l_base_personal (id, cedula, nombres, apellidos, correo, telefono, fecha_nacimiento, direccion, fuente, dpto, cargo, fecha_lectura, horario, status) FROM stdin;
13	19685352	ññññññ	ASDFSDFA	yonalmarr@gmail.com	4143352689	1936-02-06	sdf	asd	asd	asd	1937-02-05	asd	9
8	18557252	YONALDY	MARRERO	yonalmarr@gmail.com	4143327226	1915-03-20	Guatire	Bumeran	SAC	Represente	1914-06-09	DIurno	9
11	19633121	ANDRES	MARRERO	yonalmarr@gmail.com	5555555	1913-05-26	asd	asdf	asdf	asd	1914-05-27	asd	9
15	21103525	PERENSEJA	FULANITO	yonalmarr@gmail.com	4143327226	2016-01-04	Guatire	Bumeran	SAC	Representante	2016-09-26	7:00 a 1:00 	9
12	25656966	MARIA	MOññ±ITOS	yonalmarr@gmail.com	4242968888	1913-05-25	Guatire	Correo	XXXX	XXXX	1914-06-16	XXXX	9
9	21103524	ANGELES 	MONTILLA	yonalmarr@gmail.com	4143327226	1906-10-15	Guatire	Bumeran	TELESALES	AGENTE	1932-10-26	3:00pm a 9:00pm	6
14	4563546	ÑLGHLGLÑ	ÑHKJHKÑ	yonalmarr@gmail.com	2342345	1936-02-06	sdfg	sdfg	sdf	sdf	1937-02-05	sdf	6
16	17896453	PRUEBA	P	yonalmarr@gmail.com	123456	1908-04-07	asdfasd	asdfas	asd	asda	1909-04-07	as	6
17	9554684	123	SDF	yonalmarr@gmail.com	414123456	1931-03-09	xxf	fdgh	dfghdf	gdfg	1910-04-08	dfg	9
19	13864945	JESUS	MARIN	jmarin@fasf.com	41433285896	2016-11-02	dfsd	2	1	1	2016-11-01	1	0
18	99645432	PRUEBA 	PRUEBA	yonalmarr@gmail.com	42429128282	1907-05-09	fsdf	2	1	1	1914-05-09	1	6
\.


--
-- Name: l_base_personal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('l_base_personal_id_seq', 19, true);


--
-- Data for Name: l_captacion; Type: TABLE DATA; Schema: public; Owner: jorgerengifo
--

COPY l_captacion (id, descripcion) FROM stdin;
2	bumeran
1	LINKL DN
\.


--
-- Name: l_captacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jorgerengifo
--

SELECT pg_catalog.setval('l_captacion_id_seq', 3, true);


--
-- Data for Name: l_cargo; Type: TABLE DATA; Schema: public; Owner: jorgerengifo
--

COPY l_cargo (id, descripcion) FROM stdin;
1	Representante
\.


--
-- Name: l_cargo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jorgerengifo
--

SELECT pg_catalog.setval('l_cargo_id_seq', 1, true);


--
-- Data for Name: l_depto; Type: TABLE DATA; Schema: public; Owner: jorgerengifo
--

COPY l_depto (id, descripcion) FROM stdin;
1	SAC
\.


--
-- Name: l_depto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jorgerengifo
--

SELECT pg_catalog.setval('l_depto_id_seq', 1, true);


--
-- Data for Name: l_fase_i; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY l_fase_i (id, id_personal, fecha_entrevista, hora, activo, asistio, entr_depto, entr_rrhh, redaccion, ortografia, observaciones) FROM stdin;
17	13	2016-09-07	10:00 am	f	t	f	f	f	f	h
16	14	2016-11-15	08:00 am	f	\N	\N	\N	\N	\N	
18	8	2016-10-11	08:00 am	f	t	t	t	t	t	la persona no me cae
19	14	2016-05-29	08:00 am	t	t	t	t	t	t	
20	9	2016-06-30	10:00 am	t	t	t	t	t	t	
21	11	2016-06-30	10:00 am	f	t	f	f	t	t	asdf
22	15	2016-06-30	10:00 am	t	t	t	t	t	t	
23	16	2016-01-17	10:00 am	t	t	t	t	t	t	
24	0	2000-01-01	10:00	t	\N	\N	\N	\N	\N	\N
25	0	2006-07-17	08:00 am	t	\N	\N	\N	\N	\N	\N
26	0	2016-06-05	08:00 am	t	\N	\N	\N	\N	\N	\N
27	0	2016-12-09	08:00 am	t	\N	\N	\N	\N	\N	\N
28	0	2010-10-10	08:00	t	\N	\N	\N	\N	\N	\N
32	0	2016-09-21	08:00 am	t	\N	\N	\N	\N	\N	\N
33	17	2016-08-17	08:00 am	t	t	t	t	t	t	
34	18	2016-11-10	08:00 am	t	t	t	t	t	t	
\.


--
-- Name: l_fase_i_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('l_fase_i_id_seq', 34, true);


--
-- Data for Name: l_horario; Type: TABLE DATA; Schema: public; Owner: jorgerengifo
--

COPY l_horario (id, descripcion) FROM stdin;
1	1:00 pm a 7:00pm
\.


--
-- Name: l_horario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jorgerengifo
--

SELECT pg_catalog.setval('l_horario_id_seq', 1, true);


--
-- Data for Name: l_status; Type: TABLE DATA; Schema: public; Owner: jorgerengifo
--

COPY l_status (id, descripcion) FROM stdin;
0	Inicial
1	En Entrevista
9	No Elegible
5	En Capacitación
4	Esperando Capacitación
2	Esperando Examenes Médicos
3	Examenes Médicos / VPC
6	Aprobado en Capacitacion
\.


--
-- Name: l_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jorgerengifo
--

SELECT pg_catalog.setval('l_status_id_seq', 3, true);


--
-- Name: auth_event_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_event
    ADD CONSTRAINT auth_event_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_membership_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_membership
    ADD CONSTRAINT auth_membership_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: p_key_l_fase_i; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY l_fase_i
    ADD CONSTRAINT p_key_l_fase_i PRIMARY KEY (id);


--
-- Name: pkey_l_base_personal; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY l_base_personal
    ADD CONSTRAINT pkey_l_base_personal PRIMARY KEY (id);


--
-- Name: pkey_l_status; Type: CONSTRAINT; Schema: public; Owner: jorgerengifo; Tablespace: 
--

ALTER TABLE ONLY l_status
    ADD CONSTRAINT pkey_l_status PRIMARY KEY (id);


--
-- Name: auth_event_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_event
    ADD CONSTRAINT auth_event_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) ON DELETE CASCADE;


--
-- Name: auth_membership_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_membership
    ADD CONSTRAINT auth_membership_group_id_fkey FOREIGN KEY (group_id) REFERENCES auth_group(id) ON DELETE CASCADE;


--
-- Name: auth_membership_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_membership
    ADD CONSTRAINT auth_membership_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) ON DELETE CASCADE;


--
-- Name: auth_permission_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_group_id_fkey FOREIGN KEY (group_id) REFERENCES auth_group(id) ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

