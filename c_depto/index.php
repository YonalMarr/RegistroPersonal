 <?php include '../home/head.php'; ?>

<script type="text/javascript">
  function newRecord(){
    $('#dlg1').dialog('open').dialog('setTitle','Nuevo');
    $('#fm1').form('clear');
    $('#btnaceptar').linkbutton('enable');
    $('#guardado2').hide();     
    read_noly = 0;
    esta_global = 0;
  }

  function editRecord(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
      $('#dlg1').dialog('open').dialog('setTitle','Modificar');
      esta_global = 1;
      $('#buscar').html('');
      $('#btnaceptar').linkbutton('enable');
      $('#fm1').form('load',row);
      id = row.id;
    }
    read_noly = 1;
    esta_global = 1;
  }
  
  function removeRecord(){
    var row = $('#dg').datagrid('getSelected');
    if (row){
      $.messager.confirm('Confirmaci&oacute;n','Est&aacute; usted seguro de eliminar este registro?',function(r){
        if (r){
          $.post('remove.php',{id:row.id},function(result){
            if (result.success){
              $('#dg').datagrid('reload');  // reload the user data
            } else {
              $.messager.alert('Error',result.msg,'error');
            }
          },'json');
        }
      });
    }
  } 
  
  function saveRecord(){
    if($('#descripcion').textbox('isValid') == false){
      rojo(2);
      $('#guardado2').html('Ingrese el nombres del Usuario');
      $('#nombre').textbox('textbox').focus();
      return false;
    }

    if(esta_global == 1){       
      $('#fm1').form('submit',{
        url: 'update.php?id='+id,
        success: function(result){
          var result = eval('('+result+')');
          if (result.success){
            $('#dlg1').dialog('close');    
            $('#dg').datagrid('reload');  
          } else {
            $.messager.show({
              title: 'Error',
              msg: result.msg
            });
          }
        }
      });
      verde(2);
      $('#guardado2').html('El registro ha sido modificado satisfactoriamente');
    }else{
      $('#fm1').form('submit',{
        url: 'save.php',
        success: function(result){
          var result = eval('('+result+')');
          if (result.success){
            $('#dlg1').dialog('close');    // close the dialog
            $('#dg').datagrid('reload');  // reload the user data
          } else {
            $.messager.show({
              title: 'Error',
              msg: result.msg
            });
          }
        }
      });
    }
  }
    function verde(num){
    $('#guardado'+num).css('font-size','14px');
    $('#guardado'+num).css('padding','5px');
    $('#guardado'+num).css('border-color','#00FF00');
    $('#guardado'+num).css('background-color','#F0FFF0');
    $('#guardado'+num).css('display','block');
    $("#guardado"+num).fadeOut(4000);
  }
      
  function rojo(num){
    $('#guardado'+num).css('font-size','14px');
    $('#guardado'+num).css('padding','5px');
    $('#guardado'+num).css('border-color','#FF0000');
    $('#guardado'+num).css('background-color','#FDE9D9');
    $('#guardado'+num).css('display','block');
    $("#guardado"+num).fadeOut(3000);
  }
   	</script>
 <?php include '../home/head_2.php'; ?>

         <div id="main" name="main" data-options="region:'center'">
            <table id="dg" 
            title="Departamentos" 
            width="100%" 
            align="center" 
            class="easyui-datagrid" 
            url="get.php" 
            fit="true" 
            toolbar="#toolbar" 
            pagination="true" 
            striped="true" 
            rownumbers="false" 
            fitColumns="true" 
            singleSelect="true"
             autoRowHeight="false" 
            data-options="fit:true, 
                           nowrap:false, 
                           fitColumns:true 
                          "
            >
               <thead data-options="frozen:true">
                  <tr>
                     <th field="descripcion" width="100%" sortable="true">Descripcion</th>
                  </tr>
               </thead>
           </table>
         <div style="display:none">
            <div id="toolbar">
               <a class="easyui-linkbutton" iconCls="icon-newdoc" plain="true" onclick="newRecord()">Nuevo</a>
               <a class="easyui-linkbutton" iconCls="icon-modify" plain="true" onclick="editRecord()">Modificar</a>
               <a class="easyui-linkbutton" iconCls="icon-erase" plain="true" onclick="removeRecord()">Eliminar</a> 

               <div id="busqueda" style="float:right">
                  <input id="criteria" name="criteria" style="width:400px" data-options="
                     prompt: 'escriba la informaci&oacute;n a buscar...',
                     icons:[{
                     iconCls:'icon-search',
                     handler: function(e){
                        $('#dg').datagrid('load',
                           {criteria:$('#criteria').textbox('getValue')}
                           );
                        }
                     }]">
               <script>
                   $.extend($.fn.textbox.methods, {
                       addClearBtn: function(jq, iconCls){
                           return jq.each(function(){
                               var t = $(this);
                               var opts = t.textbox('options');
                               opts.icons = opts.icons || [];
                               opts.icons.unshift({
                                   iconCls: iconCls,
                                   handler: function(e){
                                       $(e.data.target).textbox('clear').textbox('textbox').focus();
                                       $(this).css('visibility','hidden');
                              
                              $('#dg').datagrid('load',
                              {
                                 criteria:$('#criteria').textbox('getValue')
                              });
                                   }
                               });
                               t.textbox();
                               if (!t.textbox('getText')){
                                   t.textbox('getIcon',0).css('visibility','hidden');
                               }
                               t.textbox('textbox').bind('keyup', function(){
                                   var icon = t.textbox('getIcon',0);
                                   if ($(this).val()){
                                       icon.css('visibility','visible');
                                   } else {
                                       icon.css('visibility','hidden');
                                  }
                               });
                           });
                       }
                   });
                   
                   $(function(){
                       $('#criteria').textbox().textbox('addClearBtn', 'icon-clear');
                   });
               </script>            

               </div>
           </div>
           
           <div id="dlg1" class="easyui-dialog" style="width:400px;height:200px;padding:10px 20px; position:relative;" closed="true" modal="true" buttons="#dlg1-buttons">
              <div class="ftitle">Departamentos</div>
              <form id="fm1" method="post" novalidate autocomplete="off">
                  <div class="fitem">
                       <label>Descripcion</label>
                       <input id="descripcion" name="descripcion" class="easyui-textbox" style="height:20px; width:300px"  >
                   </div>
              </form>
          <div id="guardado2" align="center" style="border-style:solid; font:Verdana, Geneva, sans-serif; border-width:1px; border-color:#00FF00; background-color:#F0FFF0; bottom:5px; width:92%; z-index:0; position:absolute; border-spacing:inherit; font-size:14px; display:none; padding:5px;"></div>
           </div>

           <div id="dlg1-buttons">
               <a id="btnaceptar" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveRecord()">Guardar</a>
               <a class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg1').dialog('close')">Cancelar</a>
           </div>

           <div id="pie"><?php// include("../home/pie.php"); ?></div>
           	</div>
           </div>
       </div>
   </body>
   </html>