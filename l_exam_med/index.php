<?php include '../home/head.php'; ?>

       
<script type="text/javascript">
  function aprobar(){
    var rows = $('#dg').datagrid('getSelections');
    if(rows!=''){
      var max = 0;
      for(var i=0; i<rows.length; i++){
        var row = rows[i];
        parametros={
          status:"4",
          id:row.id
        }
      $.ajax({
          data:  parametros,
          url:   'status.php',
          type:  'post',   
          success:  function (data) {
            $('#dg').datagrid('reload');
          }
        });
      }
    }
  }

  function rechazar(){
    var rows = $('#dg').datagrid('getSelections');
    if(rows!=''){
      var max = 0;
      for(var i=0; i<rows.length; i++){
        var row = rows[i];
        parametros={
          status:"9",
          id:row.id,
        }
      $.ajax({
          data:  parametros,
          url:   'status.php',
          type:  'post',   
          success:  function (data) {
            $('#dg').datagrid('reload');
           }
        });
      }
    }
  }

  function exaMedicos(){
    var rows = $('#dg').datagrid('getSelections');
    if(rows!=''){
      var max = 0;
      var total = rows.length;
      $.messager.prompt('Mensaje Correo', 'Inserte la fecha y la hora del examen m&eacute;dico: (Dia, Fecha, Hora)', function(fecha_examen){
        if (fecha_examen){
          for(var i=0; i<rows.length; i++){
            var row = rows[i];
            if(row.status == 3){
              var parametros = {
                "correo": row.correo,
                "nombres":row.nombres,
                "apellidos":row.apellidos,
                "cedula":row.cedula,
                "fecha":fecha_examen,
                "id":row.id
              }
              $.ajax({
                data:  parametros,
                url:   'mail.php',
                type:  'post',   
                success:  function (data) { }
              });
            }
          }
          max = 1 + i;
          if(i == total){
            $('#dg').datagrid('reload');
          }
        }
      });
    }
  }

  function verde(num){
    $('#guardado'+num).css('font-size','14px');
    $('#guardado'+num).css('padding','5px');
    $('#guardado'+num).css('border-color','#00FF00');
    $('#guardado'+num).css('background-color','#F0FFF0');
    $('#guardado'+num).css('display','block');
    $("#guardado"+num).fadeOut(4000);
  }
  
  function rojo(num){
    $('#guardado'+num).css('font-size','14px');
    $('#guardado'+num).css('padding','5px');
    $('#guardado'+num).css('border-color','#FF0000');
    $('#guardado'+num).css('background-color','#FDE9D9');
    $('#guardado'+num).css('display','block');
    $("#guardado"+num).fadeOut(3000);
  }

  function cellStyler(value,row,index){
    if (value == 2){
      return 'background-color:#61FF86;color:#000;font-weight:bold;';
    }
  }
</script>
  <?php include '../home/head_2.php'; ?>

      <div id="main" name="main" data-options="region:'center'">
        <table id="dg" 
        title="Examenes Médicos / VPC" 
        width="70%" 
        align="center" 
        class="easyui-datagrid" 
        url="get.php" 
        fit="true" 
        toolbar="#toolbar" 
        pagination="true" 
        striped="true" 
        rownumbers="true" 
        fitColumns="true" 
        singleSelect="false" 
        data-options="fit:true, nowrap:false, fitColumns:true ">
          <thead data-options="frozen:true">
            <tr>
                                 <th data-options="field:'ck',checkbox:true"></th>

              <th field="nombre_completo" width="25%" sortable="true">Nombre</th>
              <th field="cedula" width="10%" sortable="true">C&eacute;dula</th>  
            </tr>
          </thead>
          <thead>
            <tr>       
             <th field="descripcion" width="15%" sortable="true" data-options="styler:cellStyler">Estatus</th>     
              <th field="cargo" width="15%" sortable="true">Cargo</th> 
              <th field="horario" width="15%" sortable="true">Horario</th>
             
            </tr>
          </thead>
        </table>
        <div style="display:none">
          <div id="toolbar">
          <a class="easyui-linkbutton" iconCls="icon-thumbsup" plain="true" onclick="aprobar()">Aprobar</a>
          <a class="easyui-linkbutton" iconCls="icon-thumbsdown" plain="true" onclick="rechazar()">Rechazar</a>
           <a class="easyui-linkbutton" iconCls="icon-retort" plain="true" onclick="exaMedicos()">Examenes M&eacute;dicos</a>
            <div id="busqueda" style="float:right">
              <input id="criteria" name="criteria" style="width:400px" data-options="
                     prompt: 'escriba la informaci&oacute;n a buscar...',
                     icons:[{
                     iconCls:'icon-search',
                     handler: function(e){
                        $('#dg').datagrid('load',
                           {criteria:$('#criteria').textbox('getValue')}
                           );
                        }
                     }]">
                <script>
                  $.extend($.fn.textbox.methods, {
                    addClearBtn: function(jq, iconCls){
                      return jq.each(function(){
                        var t = $(this);
                        var opts = t.textbox('options');
                        opts.icons = opts.icons || [];
                        opts.icons.unshift({
                          iconCls: iconCls,
                          handler: function(e){
                            $(e.data.target).textbox('clear').textbox('textbox').focus();
                            $(this).css('visibility','hidden');
                            $('#dg').datagrid('load',{
                              criteria:$('#criteria').textbox('getValue')
                            });
                          }
                        });
                        t.textbox();
                        if (!t.textbox('getText')){
                          t.textbox('getIcon',0).css('visibility','hidden');
                        }
                        t.textbox('textbox').bind('keyup', function(){
                          var icon = t.textbox('getIcon',0);
                          if ($(this).val()){
                            icon.css('visibility','visible');
                          } else {
                            icon.css('visibility','hidden');
                          }
                        });
                      });
                    }
                  });
                  $(function(){
                    $('#criteria').textbox().textbox('addClearBtn', 'icon-clear');
                  });
              </script>
            </div>
          </div>
      </div>
    </div>    
    </div>

  </body>
</html>