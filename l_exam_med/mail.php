<?php
header('Content-Type: text/html; charset=utf-8');

date_default_timezone_set('Etc/UTC');

require '../src/PHPMailer/PHPMailerAutoload.php';
include '../home/user_validate.php';

    $nombre=mb_strtoupper($_REQUEST['nombres']);
    $apellido=mb_strtoupper($_REQUEST['apellidos']);
    $cedula=$_REQUEST['cedula'];
    $correo=$_REQUEST['correo'];
    $celular=$_REQUEST['telefono'];
    $fecha_nac=$_REQUEST['fecha_nacimiento'];   
    $direccion=$_REQUEST['direccion'];  

    $fuente=$_REQUEST['fuente'];
    $dpto=$_REQUEST['dpto'];
    $cargo=$_REQUEST['cargo'];
    $horario=$_REQUEST['horario'];
    $fecha_lectura=$_REQUEST['fecha_lectura'];
    $status = $_REQUEST['status'];
    $idusuario = $_REQUEST['id'];
    $hora = $_REQUEST['hora'];
    $fecha_entrevista = $_REQUEST['fecha'];

//Create a new PHPMailer instance
$mail = new PHPMailer;

//Tell PHPMailer to use SMTP
$mail->isSMTP();

//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';

//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;

//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';

//Whether to use SMTP authentication
$mail->SMTPAuth = true;

//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "yonalmarr@gmail.com";

//Password to use for SMTP authentication
$mail->Password = "1701**am";

//Set who the message is to be sent from
$mail->setFrom('from@example.com', $user_global );

//Set who the message is to be sent to
$mail->addAddress($correo, $nombre);

//Set the subject line
$mail->Subject = '';
$pagina = 'https://docs.google.com/forms/d/1wm0ON3wFzhQaiU5fVbrcSRj1K9soRVs3nw5YwhukCCM/viewform?usp=send_form';
         $cuerpo  = "Buenos días,<br><br>";

            $cuerpo .=  "Abajo encontrarás la fecha e indicaciones que debes de seguir para los exámenes de pre empleo, como parte del proceso de reclutamiento de Linio.<br><br>"; 

            $cuerpo .= "Adicional a ello, te pedimos que por favor, ingreses hoy mismo al link señalado a continuación  y completes tu información personal,  esta actividad es de carácter obligatorio para poder dar continuidad a tu proceso:<br><br>";

            $cuerpo .= "<a href=".$pagina.">https://docs.google.com/forms/d/1wm0ON3wFzhQaiU5fVbrcSRj1K9soRVs3nw5YwhukCCM/viewform?usp=send_form</a><br><br>";

            $cuerpo .= "Debes asistir a los exámenes de pre empleo con los siguientes documentos:<br><br>";

            $cuerpo .= "<strong>-Original y copia de la cédula de identidad.</strong><br><br>";

            $cuerpo .= "<font color='red'>Fecha: ".$fecha_entrevista."</font><br><br>";

            $cuerpo .= "Dirección: CCCT, TORRE C, PISO 5, OFICINA 502. Tomar los ascensores que están a la derecha del Farmatodo ubicado en la Feria Nivel C1.<br><br>";

            $cuerpo .= "<center><strong><font color='red'>INFORMACIÓN PARA PACIENTES</font></strong></center><br><br>";

            $cuerpo .= "<strong> <u>Información importante Audiometrias:</u></strong> <br><br>";

            $cuerpo .= "·Tener un reposo auditivo mínimo de 12 horas.<br>
                        ·No usar audífonos 12 horas antes al examen.<br>
                        ·No montar en motocicleta 12 horas antes del examen.<br>
                        ·Preferiblemente no haber viajado durante las 12 horas anteriores del examen.<br>
                        ·No acudir a centros nocturnos o áreas de exposición de ruido.<br>
                        ·No debe realizarse cuando existan procesos infecciosos en las vías respiratorias superiores (gripe) con afectación de oído y senos para nasales, ni cuando existan tapones de cerumen.<br>
                        ·No realizar limpieza y/o lavado de oído días  antes del examen.<br><br>";

            $cuerpo .= "<strong> <u>Información importante:</u></strong> <br><br>";
            $cuerpo .= "<strong>** Tener ayuno no mayor de 10 ó 12 horas.<br><br>
 
**No ingerir bebidas alcohólicas el día anterior a su evaluación.<br><br>
 
** No traer la muestra de orina ya que se toma cuando ellos asistan a nuestras instalaciones.<br><br>
 
** Si el paciente es Hipertenso, debe tomar su tratamiento con normalidad.<br><br>
 
** Si el paciente ha sido tratado con Esteroides o con algún medicamento que lo contenga <font color='red'>no puede</font> realizar la toma de laboratorio sino hasta después de 48 horas de ello.<br><br>
 
 ** En caso de realizar Electro, si el paciente pertenece al género masculino y posee vello abundante,  le solicitamos depilar únicamente la zona del tórax.<br><br>
 
** Limitar el uso de bolsos o equipaje de gran tamaño que puedan obstruir el paso.<br><br>
 
** Evitar traer acompañantes <font color='red'>(PRINCIPALMENTE NIÑOS)</font>, a menos que los pacientes:<br><br>
 
<font color='red'>1.  Sean menores de Edad.</font><br>
2.  Personas mayores<br>
3.  Personas con dificultades de lectura/ escritura para completar planillas extensas.<br><br>
 
**La atención de los pacientes se realizará por orden de llegada. Favor tomar sus precauciones. <font color='red'>(Se hará la excepción en los Casos de Consulta Curativa, siempre y cuando su sintomatología amerite Prioridad)</font>.<br>
Por favor, recuerda confirmar la recepción del correo y tu asistencia a los <font color='red'>exámenes</font> médicos.
</strong> <br><br>";

            $body = utf8_decode($cuerpo);

            $mail->msgHTML($body);

            $mail->AltBody= $body;


if (!$mail->send()) {
   echo json_encode(array('success'=>false));
} else {

echo json_encode(array('success'=>true));// echo "Message sent!";

}


?>