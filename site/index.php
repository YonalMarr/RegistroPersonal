<?php 
	include '../home/title_define.php';
	include '../home/icon_define.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo "$page_tittle"; ?></title>
    <link rel="shortcut icon" href="<?php echo "$page_icon"; ?>">
	<link rel="stylesheet" type="text/css" href="../src/themes/material/easyui.css">
	<link rel="stylesheet" type="text/css" href="../src/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="../src/themes/demo.css">
	<link rel="stylesheet" type="text/css" href="../src/themes/color.css">
	<script type="text/javascript" src="../src/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="../src/jquery/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../src/jquery/easyui-lang-es.js"></script>
	<script type="text/javascript">
		function clearForm(){
			$('#fm').form('clear');
			$('#respuesta').html('');
			$('#idusuario').focus();
		}
		
		function validarUser()
		{
			var idusuario = $('#idusuario').val();
			var clave = $('#clave').val();
						
			if (idusuario == "" || clave == "")
			{
				$('#respuesta').css("color","red");
				$('#respuesta').html('El nombre de usuario o la clave no deben ser espacios vacíos');
				$('#idusuario').focus();
				return;
			}
	
			$.post('process_index.php?usuario='+idusuario+';'+clave,
			function(data){
				$('#respuesta').show();
				if(data==1){
					$('#respuesta').css("color","black");
					$('#respuesta').html('Login exitoso. Redireccionando...');
					$(location).attr('href','../site/home.php');
				}else{
					$('#respuesta').css("color","red");
					$('#respuesta').html('Ingrese el nombre de usuario y la clave correcta para acceder al sistema');
				}
			});
		}
		
		function validar(e) {
		  tecla = (document.all) ? e.keyCode : e.which;
		  if (tecla==13){validarUser()};
		}
    </script>    
    <style>
        html	{overflow: hidden; height: 100%;}
        body	{overflow: auto; height: 100%; width: 90%; margin: 0 auto; /* center */ padding: 10px 20px; border-width: 0 1px;}
	</style>
</head>
<body>
    <div class="easyui-layout" style="width:100%; height:100%;">
        <div data-options="region:'north',border:false" style="height:110px;padding:10px 0 10px 0;">
        	<?php include("../home/encabezado.php"); ?>
            <div id="titulo"><?php include("../home/title.php"); ?></div>
        </div>
        <div data-options="region:'south',border:false" style="height:50px;padding:10px;"><?php include("../home/pie.php"); ?></div>
        <div data-options="region:'center'"></div>
    </div>
    
        <div id="w" class="easyui-window" title="Seguridad" data-options="modal:true,closed:false,minimizable:false,maximizable:false,collapsible:false,closable:false,resizable:false,draggable:false" style="width:400px;height:230px;padding:10px;">
            <form id="fm" method="get" autocomplete="off" style="padding:10px 20px 10px 40px;">
                <div class="fitem">
                    <label>Usuario</label>
                    <input name="idusuario" id="idusuario" type="text" class="easyui-validatebox textbox"  data-options="validType:'email'" required="true" style="height:20px; width:200px;text-transform:lowercase;">
                </div>
                <div class="fitem">
                    <label>Clave</label>
                    <input name="clave" id="clave" type="password" class="easyui-validatebox textbox" data-options="required:true" onkeypress="validar(event)" style="height:20px; width:200px;">
                </div>
                <br/>
            </form>
            <div style="padding:5px;text-align:center;">  
                <a class="easyui-linkbutton" icon="icon-ok" style="width:100px" onclick="validarUser()">Aceptar</a>  
                <a class="easyui-linkbutton" icon="icon-cancel" style="width:100px" onclick="clearForm()">Cancelar</a>  
            </div>
            <br/>
            <div id="respuesta" style="text-align:center;"></div>
        </div>
</body>
</html>