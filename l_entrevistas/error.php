<?php 
    include '../home/title_define.php';
    include '../home/icon_define.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo "$page_tittle"; ?></title>
    <link rel="shortcut icon" href="<?php echo "$page_icon"; ?>">
    <link rel="stylesheet" type="text/css" href="../src/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="../src/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="../src/themes/demo.css">
    <script type="text/javascript" src="../src/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../src/jquery/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="../src/jquery/easyui-lang-es.js"></script>
    <style>
        about_title{
            color:#534F4F;
            font-family:Arial, Helvetica, Verdana;
            font-size:19px;
            font-weight:bold;
            text-align:center;
        }
        err_title{
            color:#FF0000;
            font-family:Arial, Helvetica, Verdana;
            font-size:12px;
            font-weight:bold;
            text-align:center;
        }
        err_explanation{
            color:#000066;
            font-family:Arial, Helvetica, Verdana;
            font-size:12px;
            text-align:center;
        }
        error_msg{
            color:#FF0000;
            font-family:Arial, Helvetica, Verdana;
            font-size:22px;
            font-weight:bold;
            text-align:center;
        }
        copy_right{
            color:#000066;
            font-family:Arial, Helvetica, Verdana;
            font-size:9px;
            text-align:center;
        }
    </style>
</head>
<body>
    <div id="container" style="width:96%; margin:auto" >
        <?php include("../home/encabezado.php"); ?>
        <div id="w" class="easyui-window" title="Error" data-options="modal:true,closed:false,iconCls:'icon-error',minimizable:false,maximizable:false,collapsible:false,closable:false,resizable:false,draggable:false" style="width:450px;height:365px;padding:10px;">
        <center><about_title><?php include '../home/title.php'; ?></about_title></center>
        <br/>
        <center><img src="../src/img/logo.png" width="110" height="110"/></center>
        <br/>
        <center><err_title>Ha tratado de ingresar al sistema sin registrarse o no ha habido actividad desde hace 1440 o más segundos</err_title></center>
        <br/>
        <center><err_explanation>Seleccione la página de acceso e ingrese correctamente su nombre de usuario y clave</err_explanation></center>
        <br/>
        <center><a onclick="linkPage('../site/')" class="easyui-linkbutton">Aceptar</a></center>
        </div>
    </div>
</body>
</html>