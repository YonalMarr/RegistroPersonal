<?php include '../home/head.php'; ?>

       

       
<script type="text/javascript">

  function validarEntrevistas(){
    var row = $('#dg').datagrid('getSelected');
    if (row.status == 1){
      $('#dlg').dialog('open').dialog('setTitle',row.nombre_completo +' - '+row.cedula);
      $('#fm').form('clear');
      $('#fm').form('load',{id:row.id});
    }
  }

  function noAsistio(){
    var id = document.getElementById("id").value;
    parametros = {"id":id}
    $.ajax({
      data:  parametros,
      url:   'noAsistio.php',
      type:  'post',   
      success:  function (data) { 
        $('#dlg').dialog('close');
        $('#dg').datagrid('reload');
      }
    });
  }

  function Rechazar(){
    var id = document.getElementById("id").value;
    var entr_rrhh = document.getElementById("entr_rrhh").checked;
    var entr_dpto = document.getElementById("entr_dpto").checked;
    var rotter = document.getElementById("rotter").checked;
    var ortografia = document.getElementById("ortografia").checked;
    var observacion = document.getElementById("observacion").value;
    if(observacion == ''){
      rojo(3);
      $('#guardado3').html('Coloque alguna observacion');
      return false;
    }
    parametros = {
      "id":id,
      "entr_rrhh":entr_rrhh,
      "entr_dpto":entr_dpto,
      "rotter":rotter,
      "ortografia":ortografia,
      "observacion":observacion
    }
    $.ajax({
      data:  parametros,
      url:   'rechazar.php',
      type:  'post',   
      success:  function (data) { 
        $('#dlg').dialog('close');
        $('#dg').datagrid('reload');
      }
    });
  }

  function Aprobar(){
    var id = document.getElementById("id").value;
    var entr_rrhh = document.getElementById("entr_rrhh").checked;
    var entr_dpto = document.getElementById("entr_dpto").checked;
    var rotter = document.getElementById("rotter").checked;
    var ortografia = document.getElementById("ortografia").checked;
    var observacion = document.getElementById("observacion").value;
    if(observacion == '' && entr_dpto == false){
      rojo(3);
      $('#guardado3').html('Coloque alguna observacion');
      return false;
      }
    parametros = {
      "id":id,
      "entr_rrhh":entr_rrhh,
      "entr_dpto":entr_dpto,
      "rotter":rotter,
      "ortografia":ortografia,
      "observacion":observacion
    }
    $.ajax({
      data:  parametros,
      url:   'aprobar.php',
      type:  'post',   
      success:  function (data) { 
        $('#dlg').dialog('close');
        $('#dg').datagrid('reload');
      }
    });
  }

  function exaMedicos(){
    var rows = $('#dg').datagrid('getSelections');
    if(rows!=''){
      var max = 0;
      var total = rows.length;
      $.messager.prompt('Mensaje Correo', 'Inserte la fecha y la hora del examen m&eacute;dico: (Dia, Fecha, Hora)', function(fecha_examen){
        if (fecha_examen){
          for(var i=0; i<rows.length; i++){
            var row = rows[i];
            if(row.status == 2){
              var parametros = {
                "correo": row.correo,
                "nombres":row.nombres,
                "apellidos":row.apellidos,
                "cedula":row.cedula,
                "fecha":fecha_examen,
                "id":row.id
              }
              $.ajax({
                data:  parametros,
                url:   'mail.php',
                type:  'post',   
                success:  function (data) { }
              });
              $.ajax({
                data:  parametros,
                url:   'status.php',
                type:  'post',   
                success:  function (data) { }
              }); 
            }
          }
          max = 1 + i;
          if(i == total){
            $('#dg').datagrid('reload');
          }
        }
      });
    }
  }

  function verde(num){
    $('#guardado'+num).css('font-size','14px');
    $('#guardado'+num).css('padding','5px');
    $('#guardado'+num).css('border-color','#00FF00');
    $('#guardado'+num).css('background-color','#F0FFF0');
    $('#guardado'+num).css('display','block');
    $("#guardado"+num).fadeOut(4000);
  }
  
  function rojo(num){
    $('#guardado'+num).css('font-size','14px');
    $('#guardado'+num).css('padding','5px');
    $('#guardado'+num).css('border-color','#FF0000');
    $('#guardado'+num).css('background-color','#FDE9D9');
    $('#guardado'+num).css('display','block');
    $("#guardado"+num).fadeOut(3000);
  }

  function noReject(){
    var entr_dpto = document.getElementById("entr_dpto");
    if(entr_dpto.checked == true){
      document.getElementById("ortografia").checked = true;
      document.getElementById("rotter").checked = true;
      document.getElementById("entr_rrhh").checked = true;
      $('#rechazar').linkbutton('disable');
      $('#noasistio').linkbutton('disable');
    }else{
      $('#rechazar').linkbutton('enable');
    }
  }

  function cellStyler(value,row,index){
    if (value == 'Esperando Examenes Médicos'){
      return 'background-color:#61FF86;color:#000;font-weight:bold;';
    }
  }
</script>
<?php include '../home/head_2.php'; ?>
<div id="main" name="main" data-options="region:'center'">
  <table id="dg" 
        title="Entrevistas" 
        width="70%" 
        align="center" 
        class="easyui-datagrid" 
        url="get.php" 
        fit="true" 
        toolbar="#toolbar" 
        pagination="true" 
        striped="true" 
        rownumbers="true" 
        fitColumns="true" 
        singleSelect="false" 
        data-options="fit:true, nowrap:false, fitColumns:true ">
          <thead data-options="frozen:true">
            <tr>
                                 <th data-options="field:'ck',checkbox:true"></th>

              <th field="nombre_completo" width="25%" sortable="true">Nombre</th>
              <th field="cedula" width="10%" sortable="true">C&eacute;dula</th> 
              <th field="fecha_entrevista" width="20%" sortable="false">Fecha de Entrevista</th> 
            </tr>
          </thead>
          <thead>
            <tr>           
              <th field="cargo" width="15%" sortable="true">Cargo</th> 
              <th field="descripcion" width="30%" sortable="true" data-options="styler:cellStyler">Estatus</th>              
            </tr>
          </thead>
        </table>
        <div style="display:none">
          <div id="toolbar">
            <a class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="validarEntrevistas()">Validar</a>
            <a class="easyui-linkbutton" iconCls="icon-retort" plain="true" onclick="exaMedicos()">Examenes M&eacute;dicos</a>
            <div id="busqueda" style="float:right">
              <input id="criteria" name="criteria" style="width:400px" data-options="
                     prompt: 'escriba la informaci&oacute;n a buscar...',
                     icons:[{
                     iconCls:'icon-search',
                     handler: function(e){
                        $('#dg').datagrid('load',
                           {criteria:$('#criteria').textbox('getValue')}
                           );
                        }
                     }]">
                <script>
                  $.extend($.fn.textbox.methods, {
                    addClearBtn: function(jq, iconCls){
                      return jq.each(function(){
                        var t = $(this);
                        var opts = t.textbox('options');
                        opts.icons = opts.icons || [];
                        opts.icons.unshift({
                          iconCls: iconCls,
                          handler: function(e){
                            $(e.data.target).textbox('clear').textbox('textbox').focus();
                            $(this).css('visibility','hidden');
                            $('#dg').datagrid('load',{
                              criteria:$('#criteria').textbox('getValue')
                            });
                          }
                        });
                        t.textbox();
                        if (!t.textbox('getText')){
                          t.textbox('getIcon',0).css('visibility','hidden');
                        }
                        t.textbox('textbox').bind('keyup', function(){
                          var icon = t.textbox('getIcon',0);
                          if ($(this).val()){
                            icon.css('visibility','visible');
                          } else {
                            icon.css('visibility','hidden');
                          }
                        });
                      });
                    }
                  });
                  $(function(){
                    $('#criteria').textbox().textbox('addClearBtn', 'icon-clear');
                  });
              </script>
            </div>
          </div>
          <div id="dlg" class="easyui-dialog" style="width:300px;height:450px;padding:10px 20px; position:relative;" closed="true" modal="true" buttons="#dlg-buttons">
            <div class="ftitle">Resultados de Entrevista</div>
            <form id="fm" method="post">
              <input id="id" name="id" type="hidden" 
                >
                <div class="fitem">
                  <label>Ortografía</label>
                  <input type="checkbox" id="ortografia" name="ortografia"><br><br>
                  <label>Rotter</label>
                  <input type="checkbox" id="rotter" name="rotter"><br><br>
                  <label>Entrevista por RRHH</label>
                  <input type="checkbox" id="entr_rrhh" name="entr_rrhh"><br><br>
                  <label>Entrevista por Departamento</label>
                  <input type="checkbox" id="entr_dpto" name="entr_dpto" onchange="noReject()"><br><br><br><br>
                  <label>Observaciones</label>
                  <input class="easyui-textbox" id="observacion" name="observacion" multiline="true" style="width:200px;height:100px"  data-options="onChange: function(re){}">
                </div>
            </form>
            <div id="guardado3" align="center" style="border-style:solid; font:Verdana, Geneva, sans-serif; border-width:1px; border-color:#00FF00; background-color:#F0FFF0; bottom:5px; width:80%; z-index:0; position:absolute; border-spacing:inherit; font-size:14px; display:none; padding:5px;"></div>
          </div>
          <div id="dlg-buttons" align="center">
            <a id="aprobar" class="easyui-linkbutton" iconCls="icon-ok" onclick="Aprobar()">Aprobar</a>
            <a id="rechazar" class="easyui-linkbutton" iconCls="icon-erase" onclick="Rechazar()">Rechazar</a>
            <a id="noasistio" class="easyui-linkbutton" iconCls="icon-problem" onclick="noAsistio()">No Asistió</a>
          </div>
        </div>
      </div>
  </body>
</html>